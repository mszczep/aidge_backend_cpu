#ifndef AIDGE_UTILS_SYS_INFO_CPU_VERSION_INFO_H
#define AIDGE_UTILS_SYS_INFO_CPU_VERSION_INFO_H

#include "aidge/utils/Log.hpp"

namespace Aidge {

#ifndef PROJECT_VERSION // Normally defined in CMakeLists.txt
#define PROJECT_VERSION "Unknown version"
#endif
#ifndef GIT_COMMIT_HASH
#define GIT_COMMIT_HASH ""
#endif
void showCpuVersion() {
    Log::info("Aidge backend CPU: {} ({}), {} {}", PROJECT_VERSION, GIT_COMMIT_HASH, __DATE__, __TIME__);
        // Compiler version
    #if defined(__clang__)
    /* Clang/LLVM. ---------------------------------------------- */
        Log::info("Clang/LLVM compiler version: {}.{}.{}\n", __clang_major__ , __clang_minor__, __clang_patchlevel__);
    #elif defined(__ICC) || defined(__INTEL_COMPILER)
    /* Intel ICC/ICPC. ------------------------------------------ */
        Log::info("Intel ICC/ICPC compiler version: {}\n", __INTEL_COMPILER);
    #elif defined(__GNUC__) || defined(__GNUG__)
    /* GNU GCC/G++. --------------------------------------------- */
        Log::info("GNU GCC/G++ compiler version: {}.{}.{}", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__);
    #elif defined(_MSC_VER)
    /* Microsoft Visual Studio. --------------------------------- */
        Log::info("Microsoft Visual Studio compiler version: {}\n", _MSC_VER);
    #else
        Log::info("Unknown compiler\n");
    #endif

}
}  // namespace Aidge
#endif  // AIDGE_UTILS_SYS_INFO_CPU_VERSION_INFO_H
