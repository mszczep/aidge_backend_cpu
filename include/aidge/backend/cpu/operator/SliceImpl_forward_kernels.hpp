/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SLICEIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_SLICEIMPL_FORWARD_KERNEL_H_

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <iterator>

#include "aidge/utils/Registrar.hpp"
#include "aidge/backend/cpu/operator/SliceImpl.hpp"

namespace Aidge {

template<class I, class O>
void SliceImpl_cpu_forward_kernel(const std::vector<std::int64_t>& starts,
                                const std::vector<std::int64_t>& ends,
                                const std::vector<std::int8_t>& axes,
                                const std::vector<std::int64_t>& steps,
                                const std::vector<DimSize_t>& inputDims,
                                const void* input_,
                                void* output_)
{
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    const std::size_t nbDims = inputDims.size();
    std::vector<DimSize_t> dims = inputDims;
    DimSize_t totalSize = std::accumulate(inputDims.cbegin(), inputDims.cend(), std::size_t(1), std::multiplies<std::size_t>());
    const I* inputAccumulation = input;
    I* outputAccumulation = nullptr;
    const std::size_t nbAxes = starts.size();
    for (std::size_t i = 0; i < nbAxes; ++i) {
        const DimIdx_t axis = axes[i] >= 0 ?
                                    static_cast<DimIdx_t>(axes[i]) :
                                    static_cast<DimIdx_t>(axes[i] + static_cast<DimIdx_t>(inputDims.size()));
        const DimSize_t start = std::min(starts[i] >= 0 ?
                                                static_cast<DimSize_t>(starts[i]) :
                                                static_cast<DimSize_t>(starts[i] + static_cast<std::int64_t>(inputDims[axis])),
                                         dims[axis]-1);
        const DimSize_t end = ends[i] >= 0 ?
                                        static_cast<DimSize_t>(ends[i]) :
                                        static_cast<DimSize_t>(ends[i] + static_cast<std::int64_t>(inputDims[axis]));
        const std::int64_t step = steps[i];

        const std::size_t sliceSize = static_cast<std::size_t>(std::ceil((static_cast<float>(end) - static_cast<float>(start)) / static_cast<float>(step)));

        outputAccumulation = new I[totalSize];
        const std::size_t stride_pre = std::accumulate(dims.cbegin(), dims.cbegin() + axis, 1, std::multiplies<std::size_t>());
        const std::size_t stride_post = std::accumulate(dims.crbegin(), dims.crbegin() + nbDims -1 - axis, 1, std::multiplies<std::size_t>());
        for (std::size_t outer = 0; outer < stride_pre; ++outer)
        {
            const std::size_t idx_in = outer * stride_post * dims[axis] + start * stride_post;
            const std::size_t idx_out = outer * stride_post * sliceSize;
            std::size_t addedSlices = 0;
            for (std::size_t inner = 0; inner < sliceSize; ++inner)
            {
                std::copy_n(std::next(inputAccumulation, idx_in + inner * step * stride_post),
                            stride_post,
                            std::next(outputAccumulation, idx_out + addedSlices * stride_post));
                addedSlices++;
            }
        }
        totalSize /= dims[axis];
        totalSize *= sliceSize;
        dims[axis] = sliceSize;

        if (inputAccumulation != input) {
            delete[] inputAccumulation;
        }
        inputAccumulation = outputAccumulation;

    }
    // Copy elements from inputAccumulation to output while dividing by divisor
    std::copy_n(inputAccumulation, totalSize, output);
    if (outputAccumulation) {
        delete[] outputAccumulation;
    }
}

namespace {
static Registrar<SliceImplForward_cpu> registrarSliceImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::SliceImpl_cpu_forward_kernel<float, float>);
static Registrar<SliceImplForward_cpu> registrarSliceImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::SliceImpl_cpu_forward_kernel<int, int>);
static Registrar<SliceImplForward_cpu> registrarSliceImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::SliceImpl_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SLICEIMPL_FORWARD_KERNEL_H_ */
