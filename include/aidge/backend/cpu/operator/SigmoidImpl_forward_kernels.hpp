/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SIGMOIDIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_SIGMOIDIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/SigmoidImpl.hpp"

namespace Aidge {
template <class I, class O>
void SigmoidImpl_cpu_forward_kernel(std::size_t inputLenght,
                                    const void* input_,
                                    void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

//#pragma omp parallel for if (inputLenght > 1024)
    for (std::size_t i = 0; i < inputLenght; ++i) {
		if (input[i] > I(0)) {
			output[i] = O(1) / (O(1) + std::exp(-input[i]));
		} else {
			output[i] = std::exp(input[i]) / (O(1) + std::exp(input[i]));
		}
    }
}

namespace {
static Registrar<SigmoidImplForward_cpu> registrarSigmoidImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::SigmoidImpl_cpu_forward_kernel<float, float>);
static Registrar<SigmoidImplForward_cpu> registrarSigmoidImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::SigmoidImpl_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SIGMOIDIMPL_FORWARD_KERNEL_H_ */
