/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_ADDIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_ADDIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include <cstdint>     // std::int32_t, std::int64_t

#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/operator/AddImpl.hpp"

namespace Aidge {

template <class I, class O>
void AddImpl_cpu_forward_kernel(const std::vector<const void*> inputs_, const std::vector<std::vector<std::size_t>>& inputDims, const std::size_t outputLength, const std::vector<std::size_t>& outDims, void* output_) {
    // FIXME: missing Add attributes as arguments
    std::vector<const I*> inputs;
    for (const auto& input_ : inputs_) {
        inputs.push_back(static_cast<const I*>(input_));
    }
    O* output = static_cast<O*>(output_);

	for (std::size_t oIndex = 0; oIndex < outputLength; ++oIndex)
	{
        output[oIndex] = 0;
		std::vector<size_t> indexes = getMultiDimIndices(outDims, oIndex);
		for(std::size_t iIndex = 0; iIndex < inputs.size(); ++iIndex) {
			std::size_t idx = getFlattenedIndex(inputDims[iIndex], indexes);
            output[oIndex] += inputs[iIndex][idx];
		}
	}
}

namespace {
static Registrar<AddImplForward_cpu> registrarAddImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::AddImpl_cpu_forward_kernel<float, float>);
static Registrar<AddImplForward_cpu> registrarAddImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::AddImpl_cpu_forward_kernel<double, double>);
static Registrar<AddImplForward_cpu> registrarAddImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::AddImpl_cpu_forward_kernel<std::int32_t, std::int32_t>);
static Registrar<AddImplForward_cpu> registrarAddImplForward_cpu_Int64(
        {DataType::Int64, DataType::Int64}, Aidge::AddImpl_cpu_forward_kernel<std::int64_t, std::int64_t>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_ADDIMPL_CPU_FORWARD_KERNEL_H_ */