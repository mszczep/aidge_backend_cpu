/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_DIVIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_DIVIMPL_FORWARD_KERNEL_H_

#include <numeric>     // std::accumulate
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int32_t, std::int64_t
#include <functional>  // std::multiplies

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/operator/DivImpl.hpp"

namespace Aidge {
// template <class I1, class I2, class O>
// void DivImpl_cpu_forward_kernel(const std::vector<std::size_t>& input1Dims,
//                                 const std::vector<std::size_t>& input2Dims,
//                                 const std::vector<std::size_t>& outputDims,
//                                 const void* input1_,
//                                 const void* input2_,
//                                 void* output_) {

//     const I1* input_1 = static_cast<const I1*>(input1_);
//     const I2* input_2 = static_cast<const I2*>(input2_);
//     O* output = static_cast<O*>(output_);

//     const std::size_t totalElements = std::accumulate(outputDims.cbegin(), outputDims.cend(), std::size_t(1), std::multiplies<std::size_t>());

// 	for (std::size_t oIndex = 0; oIndex < totalElements; ++oIndex)
// 	{
// 		std::vector<std::size_t> indexes = getMultiDimIndices(outputDims, oIndex);

// 		std::size_t idx1 = getFlattenedIndex(input1Dims, indexes);
// 		std::size_t idx2 = getFlattenedIndex(input2Dims, indexes);

//         // TODO assert if input_2 is bad?
//         output[oIndex] = input_1[idx1] / input_2[idx2];
//     }
// }

template <class I1, class I2, class O>
constexpr void DivImpl_cpu_forward_kernel(const std::size_t input1size_,
                                const std::size_t input2size_,
                                const std::size_t output1size_,
                                const void* input1_,
                                const void* input2_,
                                void* output_) {

    const I1* input_1 = static_cast<const I1*>(input1_);
    const I2* input_2 = static_cast<const I2*>(input2_);
    O* output = static_cast<O*>(output_);

// suppose values are contiguous in memory
    for (std::size_t i = 0; i < output1size_; ++i) {
        const std::size_t in1_id = (input1size_ != 1) ? i : 0;
        const std::size_t in2_id = (input2size_ != 1) ? i : 0;
        output[i] = static_cast<O>(input_1[in1_id] / input_2[in2_id]);
    }
}



namespace {
static Registrar<DivImplForward_cpu> registrarDivImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::DivImpl_cpu_forward_kernel<float, float, float>);
static Registrar<DivImplForward_cpu> registrarDivImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::DivImpl_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t>);
static Registrar<DivImplForward_cpu> registrarDivImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::DivImpl_cpu_forward_kernel<double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_DIVIMPL_FORWARD_KERNEL_H_ */
