/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_
#define AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Softmax.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// class Softmax_Op;

// compute kernel registry for forward and backward
class SoftmaxImplForward_cpu
    : public Registrable<SoftmaxImplForward_cpu, std::tuple<DataType, DataType>, void(std::size_t, const std::vector<DimSize_t>&, const void*, void*)> {
};
class SoftmaxImplBackward_cpu
    : public Registrable<SoftmaxImplBackward_cpu, std::tuple<DataType, DataType>, void(std::size_t, const std::vector<DimSize_t>&, const void*, void*)> {
};

class SoftmaxImpl_cpu : public OperatorImpl {
public:
    SoftmaxImpl_cpu(const Softmax_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<SoftmaxImpl_cpu> create(const Softmax_Op& op) {
        return std::make_unique<SoftmaxImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
static Registrar<Softmax_Op> registrarSoftmaxImpl_cpu("cpu", Aidge::SoftmaxImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SOFTMAXIMPL_H_ */
