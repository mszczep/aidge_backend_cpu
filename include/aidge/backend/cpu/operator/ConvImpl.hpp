/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVIMPL_H_
#define AIDGE_CPU_OPERATOR_CONVIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class Conv_Op;

// compute kernel registry for forward and backward
// Conv 1D
class ConvImpl1DForward_cpu
    : public Registrable<ConvImpl1DForward_cpu,
                         std::tuple<DataType, DataType, DataType, DataType>,
                         void(const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 1>&,
                            const std::array<DimSize_t, 3> &,
                            DimSize_t,
                            const void *,
                            const void *,
                            const void *,
                            void *)> {};

class ConvImpl1D_cpu : public OperatorImpl {
   public:
    ConvImpl1D_cpu(const Conv_Op<1>& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<ConvImpl1D_cpu> create(const Conv_Op<1> &op) {
        return std::make_unique<ConvImpl1D_cpu>(op);
    }

   public:
    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to Conv_Op<1> implementation registry
static Registrar<Conv_Op<1>> registrarConvImpl1D_cpu("cpu", Aidge::ConvImpl1D_cpu::create);
}  // namespace

// Conv 2D
class ConvImpl2DForward_cpu
    : public Registrable<ConvImpl2DForward_cpu,
                         std::tuple<DataType, DataType, DataType, DataType>,
                         void(const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 4> &,
                            DimSize_t,
                            const void *,
                            const void *,
                            const void *,
                            void *)> {};
class ConvImpl2DBackward_cpu
    : public Registrable<ConvImpl2DBackward_cpu,
                         std::tuple<DataType, DataType, DataType, DataType>,
                         void(const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            bool,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            const void *,
                            const void *,
                            void *)> {};

class ConvImpl2D_cpu : public OperatorImpl {
   public:
    ConvImpl2D_cpu(const Conv_Op<2>& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<ConvImpl2D_cpu> create(const Conv_Op<2> &op) {
        return std::make_unique<ConvImpl2D_cpu>(op);
    }

   public:
    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to Conv_Op<2> implementation registry
static Registrar<Conv_Op<2>> registrarConvImpl2D_cpu("cpu", Aidge::ConvImpl2D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVIMPL_H_ */
