/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SLICEIMPL_H__
#define AIDGE_CPU_OPERATOR_SLICEIMPL_H__

#include <memory>
#include <vector>
#include <array>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Slice.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class Slice_Op;

// compute kernel registry for forward and backward
class SliceImplForward_cpu
    : public Registrable<SliceImplForward_cpu,
                        std::tuple<DataType, DataType>,
                        void(const std::vector<std::int64_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<std::int8_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<DimSize_t>&,
                            const void*,
                            void*)> {};
class SliceImplBackward_cpu
    : public Registrable<SliceImplBackward_cpu,
                        std::tuple<DataType, DataType>,
                        void(const std::vector<std::int64_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<std::int8_t>&,
                            const std::vector<std::int64_t>&,
                            const std::vector<DimSize_t>&,
                            const void*,
                            void*)> {};

class SliceImpl_cpu : public OperatorImpl {
public:
    SliceImpl_cpu(const Slice_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<SliceImpl_cpu> create(const Slice_Op& op) {
        return std::make_unique<SliceImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
static Registrar<Slice_Op> registrarSliceImpl_cpu("cpu", Aidge::SliceImpl_cpu::create);
}
}  // namespace Aidge

#endif /* __AIDGE_CPU_OPERATOR_SLICEIMPL_H__ */
