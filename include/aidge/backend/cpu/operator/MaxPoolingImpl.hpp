/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_H_
#define AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class MaxPooling_Op;

// compute kernel registry for forward and backward
class MaxPoolingImpl2DForward_cpu
    : public Registrable<MaxPoolingImpl2DForward_cpu,
                        std::tuple<DataType, DataType>,
                        void(const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const bool,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            void *)> {};
class MaxPoolingImpl2DBackward_cpu
    : public Registrable<MaxPoolingImpl2DBackward_cpu,
                        std::tuple<DataType, DataType>,
                        void(const std::array<DimSize_t, 2>&,
                            const std::array<DimSize_t, 2>&,
                            const bool,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            void *)> {};

class MaxPoolingImpl2D_cpu : public OperatorImpl {
public:
    MaxPoolingImpl2D_cpu(const MaxPooling_Op<2> &op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<MaxPoolingImpl2D_cpu> create(const MaxPooling_Op<2> &op) {
        return std::make_unique<MaxPoolingImpl2D_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to MaxPooling_Op<2> implementation registry
static Registrar<MaxPooling_Op<2>> registrarMaxPoolingImpl2D_cpu("cpu", Aidge::MaxPoolingImpl2D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_H_ */
