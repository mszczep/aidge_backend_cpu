/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_FORWARD_KERNEL_H_

#include <array>
#include <tuple>
#include <cmath>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/AvgPoolingImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Forward kernel for 2D AvgPoolingolution on CPU backend.
 * @tparam I Input data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class O>
void AvgPoolingImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 2>& strideDims,
                                        const std::array<DimSize_t, 2>& kernelDims,
                                        const std::array<DimSize_t, 4> &dims,
                                        const void *input_,
                                        void *output_) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);


    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[2] - kernelDims[0] + strideDims[0]) /
                                static_cast<float>(strideDims[0])));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[3] - kernelDims[1] + strideDims[1]) /
                                static_cast<float>(strideDims[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < dims[1]; ++ch) {
            const std::size_t oIndex = (ch + batch*dims[1]) * oxSize * oySize;
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2] * dims[3];
            std::fill(output + oIndex, output+(oIndex+oxSize*oySize), 0);
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                const signedsize difx = static_cast<signedsize>(- ox * strideDims[0]);
                const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                const std::size_t sxMax = (static_cast<signedsize>(dims[2]) + difx) < 0 ? 0 : ((dims[2] + difx) > kernelDims[0] ? kernelDims[0] : dims[2] + difx);
                for (std::size_t oy = 0; oy < oySize; ++oy) {
                    const signedsize dify = static_cast<signedsize>(- oy * strideDims[1]);
                    const std::size_t syMin = static_cast<std::size_t>(std::max(dify, signedsize(0)));
                    const std::size_t syMax = (static_cast<signedsize>(dims[3]) + dify) < 0 ? 0 : ((dims[3] + dify) > kernelDims[1] ? kernelDims[1] : dims[3] + dify);
                    const std::size_t oIndexFull = oIndex + ox*oySize + oy;
                    const std::size_t ix = ox * strideDims[0];
                    const std::size_t iy = oy * strideDims[1];

                    if (sxMin == 0 && syMin == 0 && sxMax == 3 && syMax == 3) {
                        output[oIndexFull] += static_cast<O>(
                                               input[iIndex + (ix+0)*dims[3] + (iy+0)] +
                                               input[iIndex + (ix+0)*dims[3] + (iy+1)] +
                                               input[iIndex + (ix+0)*dims[3] + (iy+2)] +
                                               input[iIndex + (ix+1)*dims[3] + (iy+0)] +
                                               input[iIndex + (ix+1)*dims[3] + (iy+1)] +
                                               input[iIndex + (ix+1)*dims[3] + (iy+2)] +
                                               input[iIndex + (ix+2)*dims[3] + (iy+0)] +
                                               input[iIndex + (ix+2)*dims[3] + (iy+1)] +
                                               input[iIndex + (ix+2)*dims[3] + (iy+2)]) / O(9);
                    } else {
                        for (std::size_t sx = sxMin; sx < sxMax; ++sx) {
                            for (std::size_t sy = syMin; sy < syMax; ++sy) {
                                output[oIndexFull] += input[iIndex + (ix+sx)*dims[3] + (iy+sy)];
                            }
                        }
                        // padding not used
                        output[oIndexFull] /= (sxMax - sxMin) * (syMax - syMin);
                    }
                }
            }
        }
    }
}

namespace {
static Registrar<AvgPoolingImpl2DForward_cpu> registrarAvgPoolingImpl2DForward_cpu_Float32(
        std::tuple<DataType, DataType>({DataType::Float32, DataType::Float32}),
        Aidge::AvgPoolingImpl2D_cpu_forward_kernel<float, float>);
static Registrar<AvgPoolingImpl2DForward_cpu> registrarAvgPoolingImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32},
        Aidge::AvgPoolingImpl2D_cpu_forward_kernel<int, int>);
static Registrar<AvgPoolingImpl2DForward_cpu> registrarAvgPoolingImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64},
        Aidge::AvgPoolingImpl2D_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_AVGPOOLINGIMPL_FORWARD_KERNEL_H_ */
