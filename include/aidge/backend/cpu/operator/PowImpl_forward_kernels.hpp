/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_POWIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_POWIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"
#include <cmath>

#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/operator/PowImpl.hpp"

namespace Aidge {
template <class I1, class I2, class O>
void PowImpl_cpu_forward_kernel(const std::vector<std::size_t>& input1Dims,
                                const std::vector<std::size_t>& input2Dims,
                                const std::vector<std::size_t>& outputDims,
                                const void* input1_,
                                const void* input2_,
                                void* output_) {

    const I1* input_1 = static_cast<const I1*>(input1_);
    const I2* input_2 = static_cast<const I2*>(input2_);
    O* output = static_cast<O*>(output_);

    size_t totalElements = 1;
    for (size_t dimSize : outputDims) {
        totalElements *= dimSize;
    }

	for (std::size_t oIndex = 0; oIndex < totalElements; ++oIndex) 
	{
		std::vector<size_t> indexes = getMultiDimIndices(outputDims, oIndex);

		std::size_t idx1 = getFlattenedIndex(input1Dims, indexes);
		std::size_t idx2 = getFlattenedIndex(input2Dims, indexes);
		
        output[oIndex] = std::pow(input_1[idx1], input_2[idx2]);
	}
}

namespace {
static Registrar<PowImplForward_cpu> registrarPowImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::PowImpl_cpu_forward_kernel<float, float, float>);
static Registrar<PowImplForward_cpu> registrarPowImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::PowImpl_cpu_forward_kernel<int, int, int>);
static Registrar<PowImplForward_cpu> registrarPowImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::PowImpl_cpu_forward_kernel<double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_POWIMPL_FORWARD_KERNEL_H_ */
