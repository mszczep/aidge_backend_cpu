/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_BATCHNORMIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_BATCHNORMIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/BatchNormImpl.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <array>
#include <cmath>
#include <algorithm>

namespace Aidge {
/**
 * @brief Forward kernel for 2D BatchNormolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param scale_ const scale Tensor.
 * @param shift_ const shift Tensor.
 * @param batchMean_ const mean Tensor.
 * @param batchVar_ const variance Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class P, class O>
void BatchNormImpl2D_cpu_forward_kernel(float epsilon, float momentum, const std::array<DimSize_t, 4> &dims,
                                       const void *input_, const void *scale_, const void *shift_, void *batchMean_, void *batchVar_, void *output_, const bool freeze) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const P *scale = static_cast<const P *>(scale_);
    const P *shift = static_cast<const P *>(shift_);
    P *batchMean = static_cast<P *>(batchMean_);
    P *batchVar = static_cast<P *>(batchVar_);
    O *output = static_cast<O *>(output_);

    const DimSize_t nbBatch = dims[0];
    const DimSize_t nbChannels = dims[1];
    const DimSize_t featureMapSize = dims[2]*dims[3];


    if ((freeze == true) || (momentum == 0.0f)) {
        for (std::size_t batch = 0; batch < nbBatch; ++batch) {
            for (std::size_t ch = 0; ch < nbChannels; ++ch) {
                const std::size_t ioIndex = (ch + batch*nbChannels) * featureMapSize;
                std::fill(output + ioIndex, output + ioIndex + featureMapSize, shift[ch]);
                const P var = std::sqrt(batchVar[ch] + static_cast<P>(epsilon));

                for (std::size_t feature = 0; feature<featureMapSize; ++feature) {
                    output[ioIndex + feature] += scale[ch] * (input[ioIndex + feature]-batchMean[ch]) / var;
                }
            }
        }
    } else {
        const std::size_t nbDataPerChannel = nbBatch * featureMapSize;
        for (std::size_t ch = 0; ch < nbChannels; ++ch) {
            I sum = I(0);
            I sumSquare = I(0);
            for (std::size_t batch = 0; batch < nbBatch; ++batch) {
                const std::size_t ioIndex = (ch + batch*nbChannels) * featureMapSize;
                std::fill(output + ioIndex, output + ioIndex + featureMapSize, shift[ch]);

                for (std::size_t feature = 0; feature<featureMapSize; ++feature) {
                    sum += input[ioIndex + feature];
                    sumSquare += input[ioIndex + feature] * input[ioIndex + feature];
                }
            }
            const I inputMean = sum / static_cast<I>(nbDataPerChannel);
            const I inputVar = sumSquare / static_cast<I>(nbDataPerChannel)  - inputMean*inputMean;

            batchMean[ch] = batchMean[ch]*(1-momentum) + inputMean*momentum;
            batchVar[ch] = batchVar[ch]*(1-momentum) + inputVar*(static_cast<I>(nbDataPerChannel)/static_cast<I>(nbDataPerChannel-1))*momentum;

            const P var = std::sqrt(inputVar + static_cast<P>(epsilon));
            for (std::size_t batch = 0; batch < nbBatch; ++batch) {
                const std::size_t ioIndex = (ch + batch*nbChannels) * featureMapSize;
                for (std::size_t feature = 0; feature<featureMapSize; ++feature) {
                    output[ioIndex + feature] += scale[ch] * (input[ioIndex + feature]-inputMean) / var;
                }
            }
        }
    }
}





namespace {
static Registrar<BatchNormImpl2DForward_cpu> registrarBatchNormImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::BatchNormImpl2D_cpu_forward_kernel<float, float, float>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_BATCHNORMIMPL_FORWARD_KERNEL_H_ */
