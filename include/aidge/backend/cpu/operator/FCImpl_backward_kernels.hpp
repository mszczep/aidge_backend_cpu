/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_FCIMPL_BACKWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_FCIMPL_BACKWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"
#include <algorithm>

#include "aidge/backend/cpu/operator/FCImpl.hpp"

namespace Aidge {
template <class I, class O, class W, class B>
void FCImpl_cpu_backward_kernel(const DimSize_t batchSize,
                                const DimSize_t inputFeatureSize,
                                const DimSize_t outputFeatureSize,
                                const void* input_,
                                const void* originalInput_,
                                const void* weight_,
                                void* output_,
                                void* weightGrad_,
                                void* biasesGrad_)
{
    // FIXME: missing FC attributes as arguments
    const I* input  = static_cast<const I*>(input_);
    const I* originalInput  = static_cast<const I*>(originalInput_);
    const W* weight = static_cast<const W*>(weight_);
    O* output       = static_cast<O*>(output_);
    W* weightGrad   = static_cast<W*>(weightGrad_);
    B* biasesGrad   = static_cast<B*>(biasesGrad_);


    // bias grad
    if (biasesGrad == nullptr) { // no bias
        std::fill(biasesGrad, biasesGrad + outputFeatureSize, B(0));
    } else {
        for (std::size_t o = 0; o < outputFeatureSize; ++o) { // nb outputs
            B sum{0};
            for (std::size_t b = 0; b < batchSize; ++b) {
                sum += input[b*outputFeatureSize + o];
            }
            biasesGrad[o] = sum;
        }
    }

    // weight grad
    for (std::size_t o = 0; o < outputFeatureSize; ++o) {
        for (std::size_t c = 0; c < inputFeatureSize; ++c) {
            W sum{0};
            for (std::size_t b = 0; b < batchSize; ++b) {
                sum += originalInput[b*inputFeatureSize + c]*input[b*outputFeatureSize + o];
            }
            weightGrad[o*inputFeatureSize + c] = sum;
        }
    }

    // input grad
    for (std::size_t b = 0; b < batchSize; ++b) {
        for (std::size_t c = 0; c < inputFeatureSize; ++c) {
            O sum{0};
            for (std::size_t o = 0; o < outputFeatureSize; ++o) {
                sum += weight[o*inputFeatureSize + c] * input[b*outputFeatureSize + o];
            }
            output[b*inputFeatureSize + c] = sum;
        }
    }
}


namespace {
static Registrar<FCImplBackward_cpu> registrarFCImpl2DBackward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::FCImpl_cpu_backward_kernel<float, float, float, float>);
static Registrar<FCImplBackward_cpu> registrarFCImpl2DBackward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::FCImpl_cpu_backward_kernel<int, int, int, int>);
static Registrar<FCImplBackward_cpu> registrarFCImpl2DBackward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::FCImpl_cpu_backward_kernel<double, double, double, double>);
}  // namespace

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_FCIMPL_BACKWARD_KERNEL_H_ */
