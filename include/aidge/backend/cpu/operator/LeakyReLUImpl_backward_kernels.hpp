/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_BACKWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_BACKWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include "aidge/backend/cpu/operator/LeakyReLUImpl.hpp"

namespace Aidge {
template <class I, class O>
void LeakyReLUImpl_cpu_backward_kernel(const float negativeSlope_,
                                     std::size_t inputLenght,
                                     const void* input_,
                                     void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
    const I negativeSlope = static_cast<const I>(negativeSlope_);

    for (std::size_t i = 0; i < inputLenght; ++i) {
        output[i] = (input[i] > 0) ? input[i] : negativeSlope*input[i];
    }
}

namespace {
static Registrar<LeakyReLUImplBackward_cpu> registrarLeakyReLUImplBackward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::LeakyReLUImpl_cpu_backward_kernel<float, float>);
static Registrar<LeakyReLUImplBackward_cpu> registrarLeakyReLUImplBackward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::LeakyReLUImpl_cpu_backward_kernel<int, int>);
static Registrar<LeakyReLUImplBackward_cpu> registrarLeakyReLUImplBackward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::LeakyReLUImpl_cpu_backward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LEAKYRELUIMPL_BACKWARD_KERNEL_H_ */
