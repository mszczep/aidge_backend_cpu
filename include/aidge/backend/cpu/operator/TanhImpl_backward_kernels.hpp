/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_TANHIMPL_BACKWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_TANHIMPL_BACKWARD_KERNEL_H_

#include <cstddef>  // std::size_t

#include "aidge/backend/cpu/operator/TanhImpl.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
template <class O, class GI, class GO>
void TanhImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                  const void* output_, const void* grad_output_,
			          void* grad_input_) {
    const O* output = static_cast<const O*>(output_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
    for (std::size_t i = 0; i < inputLenght; ++i) {
        grad_input[i] = (O(1) - output[i] * output[i]) * grad_output[i];
    }
}

namespace {
static Registrar<TanhImplBackward_cpu> registrarTanhImplBackward_cpu_Float32(
    {DataType::Float32, DataType::Float32, DataType::Float32},
    Aidge::TanhImpl_cpu_backward_kernel<float, float, float>);
static Registrar<TanhImplBackward_cpu> registrarTanhImplBackward_cpu_Float64(
    {DataType::Float64, DataType::Float64, DataType::Float64},
    Aidge::TanhImpl_cpu_backward_kernel<double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_TANHIMPL_BACKWARD_KERNEL_H_ */
