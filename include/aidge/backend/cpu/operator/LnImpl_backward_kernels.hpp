/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LNIMPL_BACKWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_LNIMPL_BACKWARD_KERNEL_H_

#include <cstddef>  // std::size_t

#include "aidge/backend/cpu/operator/LnImpl.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
template <class I, class GI, class GO>
void LnImpl_cpu_backward_kernel(const std::size_t inputLenght,
                                const void* input_, const void* grad_output_,
	                            void* grad_input_) {
						 
    const I* input = static_cast<const I*>(input_);
    const GO* grad_output = static_cast<const GO*>(grad_output_);
    GI* grad_input = static_cast<GI*>(grad_input_);
	const float eps = 1.0e-20f;
	
    for (std::size_t i = 0; i < inputLenght; ++i) {
		if (input[i] > I(eps)) {
			grad_input[i] = grad_output[i] / input[i];
		} else {
			grad_input[i] = GI(0);
		}
    }
}

namespace {
static Registrar<LnImplBackward_cpu> registrarLnImplBackward_cpu_Float32(
    {DataType::Float32, DataType::Float32, DataType::Float32},
    Aidge::LnImpl_cpu_backward_kernel<float, float, float>);	
static Registrar<LnImplBackward_cpu> registrarLnImplBackward_cpu_Float64(
    {DataType::Float64, DataType::Float64, DataType::Float64},
    Aidge::LnImpl_cpu_backward_kernel<double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LNIMPL_BACKWARD_KERNEL_H_ */
