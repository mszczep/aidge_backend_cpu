/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_

#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/ConvDepthWiseImpl.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Forward kernel for 1D ConvDepthWiseolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param inputDims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvDepthWiseImpl1D_cpu_forward_kernel(const std::array<DimSize_t, 1>& strideDims,
                            const std::array<DimSize_t, 1>& /*dilationDims*/,
                            const std::array<DimSize_t, 1>& kernelDims,
                            const std::array<DimSize_t, 3>& inputDims,
                            const void *input_,
                            const void *weights_,
                            const void *biases_,
                            void *output_) {
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);


    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[2] - kernelDims[0] + strideDims[0]) /
                                static_cast<float>(strideDims[0])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
        for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {
            const std::size_t oIndex = (ch + batch*inputDims[1]) * oxSize;
            B biasVal = (biases != nullptr) ? biases[ch] : B(0);
            std::fill(output + oIndex, output+(oIndex+oxSize), biasVal);
            const std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2];
            const std::size_t wIndex = ch * kernelDims[0];
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                const signedsize difx = static_cast<signedsize>(- ox * strideDims[0]);
                const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                const std::size_t sxMax = (static_cast<signedsize>(inputDims[2]) + difx) < 0 ? 0 : ((inputDims[2] + difx) > kernelDims[0] ? kernelDims[0] : inputDims[2] + difx);
                const std::size_t oIndexFull = oIndex + ox;
                const signedsize ix = static_cast<signedsize>(ox * strideDims[0]);

                for (std::size_t sx = sxMin; sx < sxMax; ++sx) {
                    output[oIndexFull] += weights[wIndex + sx] *
                                            input[iIndex + static_cast<std::size_t>(ix+static_cast<signedsize>(sx))];
                }
            }
        }
    }
}

namespace {
static Registrar<ConvDepthWiseImpl1DForward_cpu> registrarConvDepthWiseImpl1DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<float, float, float, float>);
static Registrar<ConvDepthWiseImpl1DForward_cpu> registrarConvDepthWiseImpl1DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t, std::int32_t>);
static Registrar<ConvDepthWiseImpl1DForward_cpu> registrarConvDepthWiseImpl1DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::ConvDepthWiseImpl1D_cpu_forward_kernel<double, double, double, double>);
}  // namespace


/**
 * @brief Forward kernel for 2D ConvDepthWiseolution on CPU backend.
 * @tparam I Input data type.
 * @tparam W Weight data type.
 * @tparam B Bias data type.
 * @tparam O Output data type.
 * @param params tuple of Attributes from the Operator
 * @param inputDims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param weights_ const weight Tensor.
 * @param biases_ const Biais Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class W, class B, class O>
void ConvDepthWiseImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 2>& strideDims,
                            const std::array<DimSize_t, 2>& /*dilationDims*/,
                            const std::array<DimSize_t, 2>& kernelDims,
                            const std::array<DimSize_t, 4>& inputDims,
                            const void *input_,
                            const void *weights_,
                            const void *biases_,
                            void *output_)
{
    // FIXME: missing convolution attributes as arguments
    const I *input = static_cast<const I *>(input_);
    const W *weights = static_cast<const W *>(weights_);
    const B *biases = static_cast<const B *>(biases_);
    O *output = static_cast<O *>(output_);


    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[2] - kernelDims[0] + strideDims[0]) /
                                static_cast<float>(strideDims[0])));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(inputDims[3] - kernelDims[1] + strideDims[1]) /
                                static_cast<float>(strideDims[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation attribute into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < inputDims[0]; ++batch) {
        for (std::size_t ch = 0; ch < inputDims[1]; ++ch) {
            const std::size_t oIndex = (ch + batch*inputDims[1]) * oxSize * oySize;
            B biasVal = (biases != nullptr) ? biases[ch] : B(0);
            std::fill(output + oIndex, output+(oIndex+oxSize*oySize), biasVal);
            const std::size_t iIndex = (ch + batch*inputDims[1]) * inputDims[2] * inputDims[3];
            const std::size_t wIndex = ch * kernelDims[0] * kernelDims[1];
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                const signedsize difx = static_cast<signedsize>(- ox * strideDims[0]);
                const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                const std::size_t sxMax = (static_cast<signedsize>(inputDims[2]) + difx) < 0 ? 0 : ((inputDims[2] + difx) > kernelDims[0] ? kernelDims[0] : inputDims[2] + difx);
                for (std::size_t oy = 0; oy < oySize; ++oy) {
                    const signedsize dify = static_cast<signedsize>(- oy * strideDims[1]);
                    const std::size_t syMin = static_cast<std::size_t>(std::max(dify, signedsize(0)));
                    const std::size_t syMax = (static_cast<signedsize>(inputDims[3]) + dify) < 0 ? 0 : ((inputDims[3] + dify) > kernelDims[1] ? kernelDims[1] : inputDims[3] + dify);
                    const std::size_t oIndexFull = oIndex + ox*oySize + oy;
                    const signedsize ix = static_cast<signedsize>(ox * strideDims[0]);
                    const signedsize iy = static_cast<signedsize>(oy * strideDims[1]);

                    if (sxMin == 0 && syMin == 0 && sxMax == 3 && syMax == 3) {
                        output[oIndexFull] +=  (weights[wIndex + 0*kernelDims[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+0)*inputDims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 0*kernelDims[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+0)*inputDims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 0*kernelDims[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+0)*inputDims[3] + static_cast<std::size_t>(iy+2)] +
                                                weights[wIndex + 1*kernelDims[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+1)*inputDims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 1*kernelDims[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+1)*inputDims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 1*kernelDims[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+1)*inputDims[3] + static_cast<std::size_t>(iy+2)] +
                                                weights[wIndex + 2*kernelDims[1] + 0] * input[iIndex + static_cast<std::size_t>(ix+2)*inputDims[3] + static_cast<std::size_t>(iy+0)] +
                                                weights[wIndex + 2*kernelDims[1] + 1] * input[iIndex + static_cast<std::size_t>(ix+2)*inputDims[3] + static_cast<std::size_t>(iy+1)] +
                                                weights[wIndex + 2*kernelDims[1] + 2] * input[iIndex + static_cast<std::size_t>(ix+2)*inputDims[3] + static_cast<std::size_t>(iy+2)]);
                    } else {
                        for (std::size_t sx = sxMin; sx < sxMax; ++sx) {
                            for (std::size_t sy = syMin; sy < syMax; ++sy) {
                                output[oIndexFull] += weights[wIndex + sx*kernelDims[1] + sy] *
                                                        input[iIndex + static_cast<std::size_t>(ix+static_cast<signedsize>(sx))*inputDims[3] + static_cast<std::size_t>(iy+static_cast<signedsize>(sy))];
                            }
                        }
                    }
                }
            }
        }
    }
}

namespace {
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<float, float, float, float>);
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t, std::int32_t>);
static Registrar<ConvDepthWiseImpl2DForward_cpu> registrarConvDepthWiseImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::ConvDepthWiseImpl2D_cpu_forward_kernel<double, double, double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_CONVDEPTHWISEIMPL_FORWARD_KERNEL_H_ */
