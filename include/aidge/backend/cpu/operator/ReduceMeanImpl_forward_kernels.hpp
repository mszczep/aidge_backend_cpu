/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_FORWARD_KERNEL_H_

#include <algorithm>   // std::for_each
#include <cstddef>     // std::size_t
#include <cstdint>     // std::int32_t
#include <functional>  //std::multiplies
#include <numeric>     //std::accumulate
#include <vector>

#include "aidge/backend/cpu/operator/ReduceMeanImpl.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
template <class I, class O>
void ReduceMeanImpl_cpu_forward_kernel(const std::vector<std::int32_t>& axes,
                                    DimSize_t /*keepDims*/,
                                    const std::vector<DimSize_t>& inputDims,
                                    const void* input_,
                                    void* output_) {

    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);

    const std::size_t nb_dims = inputDims.size();
    const std::size_t totalElements = std::accumulate(inputDims.cbegin(), inputDims.cend(), 1, std::multiplies<std::size_t>());

    if (axes.size() == 1) {
        const std::size_t stride_pre = std::accumulate(inputDims.cbegin(), inputDims.cbegin() + axes[0], 1, std::multiplies<std::size_t>());
        const std::size_t stride_post = std::accumulate(inputDims.crbegin(), inputDims.crbegin() + nb_dims -1 - axes[0], 1, std::multiplies<std::size_t>());

        const std::size_t dim_i = inputDims[axes[0]];
        for (std::size_t pre = 0; pre < stride_pre; ++pre) {
            for (std::size_t post = 0; post < stride_post; ++post) {
                const std::size_t idx_i = pre * dim_i * stride_post + post;
                const std::size_t idx_o = pre * stride_post + post;
                O mean = 0;
                for (std::size_t i = 0; i < dim_i; ++i) {
                    // Single pass numerically stable mean, using the fmaf
                    mean = fmaf(input[idx_i + i*stride_post] - mean, 1.0f/(i+1), mean);
                }
                output[idx_o]  = mean;
            }
        }
    } else {
        std::size_t outputElements = totalElements;

        auto stride_post = std::unique_ptr<std::size_t[]>(new std::size_t[nb_dims]);
        stride_post[nb_dims - 1] = 1;
        for (std::size_t i = nb_dims-2; i != static_cast<std::size_t>(-1); --i) {
            stride_post[i] = stride_post[i+1]*inputDims[i+1];
        }
        auto stride_pre = std::unique_ptr<std::size_t[]>(new std::size_t[nb_dims]);
        stride_pre[0] = 1;
        for (std::size_t i = 1; i < nb_dims; ++i) {
            stride_pre[i] = stride_pre[i-1]*inputDims[i-1];
        }

        const I* inputAccumulation = input;
        I* outputAccumulation = nullptr;

        for (const auto& axisInt : axes) {
            const std::size_t a = static_cast<std::size_t>(axisInt);
            outputElements /= inputDims[a];
            outputAccumulation = new I[outputElements];
            const std::size_t dim_i = inputDims[a];
            for (std::size_t pre = 0; pre < stride_pre[a]; ++pre) {
                for (std::size_t post = 0; post < stride_post[a]; ++post) {
                    const std::size_t idx_i = pre * dim_i * stride_post[a] + post;
                    const std::size_t idx_o = pre * stride_post[a] + post;
                    I mean = 0;
                    for (std::size_t i = 0; i < dim_i; ++i) {
                        // Single pass numerically stable mean, using the fmaf
                        mean = fmaf(inputAccumulation[idx_i + i*stride_post[a]] - mean, 1.0f/(i+1), mean);
                    }
                    outputAccumulation[idx_o] = mean;
                }
            }
            std::for_each(stride_pre.get()+a+1, stride_pre.get()+nb_dims, [dim_i] (std::size_t& val) { val /= dim_i; });
            if (inputAccumulation != input) {
                delete[] inputAccumulation;
            }
            inputAccumulation = outputAccumulation;
        }

        // Copy elements from inputAccumulation to output while dividing by divisor
        std::copy(inputAccumulation, inputAccumulation + outputElements, output);
        if (outputAccumulation) {
            delete[] outputAccumulation;
        }
    }
}

namespace {
static Registrar<ReduceMeanImplForward_cpu> registrarReduceMeanImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<float, float>);
static Registrar<ReduceMeanImplForward_cpu> registrarReduceMeanImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<int, int>);
static Registrar<ReduceMeanImplForward_cpu> registrarReduceMeanImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64}, Aidge::ReduceMeanImpl_cpu_forward_kernel<double, double>);

// // DIM = 1
// static Registrar<ReduceMeanImpl1DForward_cpu> registrarReduceMeanImplForward_1D_cpu_Float32(
//         {DataType::Float32, DataType::Float32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<float, float,1>);
// static Registrar<ReduceMeanImpl1DForward_cpu> registrarReduceMeanImplForward_1D_cpu_Int32(
//         {DataType::Int32, DataType::Int32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<int, int,1>);
// static Registrar<ReduceMeanImpl1DForward_cpu> registrarReduceMeanImplForward_1D_cpu_Float64(
//         {DataType::Float64, DataType::Float64}, Aidge::ReduceMeanImpl_cpu_forward_kernel<double, double,1>);

// // DIM = 2
// static Registrar<ReduceMeanImpl2DForward_cpu> registrarReduceMeanImplForward_2D_cpu_Float32(
//         {DataType::Float32, DataType::Float32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<float, float,2>);
// static Registrar<ReduceMeanImpl2DForward_cpu> registrarReduceMeanImplForward_2D_cpu_Int32(
//         {DataType::Int32, DataType::Int32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<int, int,2>);
// static Registrar<ReduceMeanImpl2DForward_cpu> registrarReduceMeanImplForward_2D_cpu_Float64(
//         {DataType::Float64, DataType::Float64}, Aidge::ReduceMeanImpl_cpu_forward_kernel<double, double,2>);

// // DIM = 3
// static Registrar<ReduceMeanImpl3DForward_cpu> registrarReduceMeanImplForward_3D_cpu_Float32(
//         {DataType::Float32, DataType::Float32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<float, float,3>);
// static Registrar<ReduceMeanImpl3DForward_cpu> registrarReduceMeanImplForward_3D_cpu_Int32(
//         {DataType::Int32, DataType::Int32}, Aidge::ReduceMeanImpl_cpu_forward_kernel<int, int,3>);
// static Registrar<ReduceMeanImpl3DForward_cpu> registrarReduceMeanImplForward_3D_cpu_Float64(
//         {DataType::Float64, DataType::Float64}, Aidge::ReduceMeanImpl_cpu_forward_kernel<double, double,3>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_REDUCEMEANIMPL_FORWARD_KERNEL_H_ */
