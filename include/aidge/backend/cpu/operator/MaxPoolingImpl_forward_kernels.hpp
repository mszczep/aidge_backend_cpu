/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_FORWARD_KERNEL_H_

#include <array>
#include <cmath>
#include <tuple>

#include "aidge/backend/cpu/operator/MaxPoolingImpl.hpp"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/data/Data.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"

namespace Aidge {
/**
 * @brief Forward kernel for 2D MaxPoolingolution on CPU backend.
 * @tparam I Input data type.
 * @tparam O Output data type.
 * @param attrs tuple of Attributes from the Operator
 * @param dims Array of input dimensions.
 * @param input_ const input Tensor.
 * @param output_ Output Tensor.
 */
template <class I, class O>
void MaxPoolingImpl2D_cpu_forward_kernel(const std::array<DimSize_t, 2>& strideDims,
                                        const std::array<DimSize_t, 2>& kernelDims,
                                        const bool /*ceilMode*/,
                                        const std::array<DimSize_t, 4> &dims,
                                        const void *input_,
                                        void *output_) {
    // FIXME: missing convolution parameters as arguments
    const I *input = static_cast<const I *>(input_);
    O *output = static_cast<O *>(output_);

    // output H size
    const std::size_t oxSize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[2] - kernelDims[0] + strideDims[0]) /
                                static_cast<float>(strideDims[0])));
    // output W size
    const std::size_t oySize =
            static_cast<std::size_t>(std::floor(static_cast<float>(dims[3] - kernelDims[1] + strideDims[1]) /
                                static_cast<float>(strideDims[1])));

    // TODO: kernel computation
    // output (batch, outCh, Xout, Yout)
    // input  (batch, ch, Xin, Yin)
    // weight (outCh, ch, kernelX, kernelY)
    // does not take Dilation parameter into account
    using signedsize = std::make_signed<std::size_t>::type;
    for (std::size_t batch = 0; batch < dims[0]; ++batch) {
        for (std::size_t ch = 0; ch < dims[1]; ++ch) {
            const std::size_t oIndex = (ch + batch*dims[1]) * oxSize * oySize;
            const std::size_t iIndex = (ch + batch*dims[1]) * dims[2] * dims[3];
            for (std::size_t ox = 0; ox < oxSize; ++ox) {
                const signedsize difx = static_cast<signedsize>(- ox * strideDims[0]);
                const std::size_t sxMin = static_cast<std::size_t>(std::max(difx, signedsize(0)));
                const std::size_t sxMax = (static_cast<signedsize>(dims[2]) + difx) < 0 ? 0 : ((dims[2] + difx) > kernelDims[0] ? kernelDims[0] : dims[2] + difx);
                for (std::size_t oy = 0; oy < oySize; ++oy) {
                    const signedsize dify = static_cast<signedsize>(- oy * strideDims[1]);
                    const std::size_t syMin = static_cast<std::size_t>(std::max(dify, signedsize(0)));
                    const std::size_t syMax = (static_cast<signedsize>(dims[3]) + dify) < 0 ? 0 : ((dims[3] + dify) > kernelDims[1] ? kernelDims[1] : dims[3] + dify);
                    const std::size_t oIndexFull = oIndex + ox*oySize + oy;
                    const std::size_t ix = ox * strideDims[0];
                    const std::size_t iy = oy * strideDims[1];

                    I poolValue(0.0);
                    bool valid = false;

                    for (unsigned int channel = 0; channel < dims[1];
                            ++channel){
                        for (unsigned int sy = syMin; sy < syMax; ++sy) {
                            for (unsigned int sx = sxMin; sx < sxMax; ++sx)
                            {
                                const I value = input[iIndex + (ix+sx)*dims[3] + (iy+sy)];

                                if (!valid || value > poolValue) {
                                    poolValue = value;
                                    valid = true;
                                }
                            }
                        }
                    }
                    output[oIndexFull] = poolValue;
                }
            }
        }
    }
}

//N2D2 version
/*
template <class T>
void N2D2::PoolCell_Frame_Kernels::forwardMax(const T* alpha,
                                              const Tensor<T>&
                                              inputs,
                                              const Descriptor& desc,
                                              const T* beta,
                                              Tensor<T>& outputs,
                                              Tensor<ArgMax>& argMax,
                                              bool useArgMax,
                                              const Tensor<bool>& maps)
{
    const unsigned int size = inputs.dimB() * outputs.dimZ();

#if defined(_OPENMP) && _OPENMP >= 200805
#pragma omp parallel for collapse(2) if (size > 16)
#else
#pragma omp parallel for if (inputs.dimB() > 4 && size > 16)
#endif
    for (int batchPos = 0; batchPos < (int)inputs.dimB(); ++batchPos) {
        for (unsigned int output = 0; output < outputs.dimZ(); ++output) {
            for (unsigned int oy = 0; oy < outputs.dimY(); ++oy) {
                for (unsigned int ox = 0; ox < outputs.dimX(); ++ox) {
                    const unsigned int sxMin = (unsigned int)std::max(
                        desc.padding[0] - (int)(ox * desc.stride[0]), 0);
                    const unsigned int syMin = (unsigned int)std::max(
                        desc.padding[1] - (int)(oy * desc.stride[1]), 0);
                    const unsigned int sxMax = Utils::clamp
                        <int>(inputs.dimX() + desc.padding[0] - ox * desc.stride[0],
                              0,
                              desc.pool[0]);
                    const unsigned int syMax = Utils::clamp
                        <int>(inputs.dimY() + desc.padding[1] - oy * desc.stride[1],
                              0,
                              desc.pool[1]);

                    const int ix = (int)(ox * desc.stride[0]) - desc.padding[0];
                    const int iy = (int)(oy * desc.stride[1]) - desc.padding[1];

                    T poolValue(0.0);

                    // For each output, compute the pool value
                    if (useArgMax) {
                        const ArgMax inputMax
                            = argMax(ox, oy, output, batchPos);

                        if (inputMax.valid) {
                            poolValue = inputs(inputMax.ix,
                                               inputMax.iy,
                                               inputMax.channel,
                                               batchPos);
                        }
                    }
                    else {
                        unsigned int ixMax = 0;
                        unsigned int iyMax = 0;
                        unsigned int channelMax = 0;
                        bool valid = false;

                        for (unsigned int channel = 0; channel < inputs.dimZ();
                             ++channel)
                        {
                            if (!maps.empty() && !maps(output, channel))
                                continue;

                            for (unsigned int sy = syMin; sy < syMax; ++sy) {
                                for (unsigned int sx = sxMin; sx < sxMax; ++sx)
                                {
                                    const T value = inputs(ix + sx,
                                                                 iy + sy,
                                                                 channel,
                                                                 batchPos);

                                    if (!valid || value > poolValue) {
                                        poolValue = value;
                                        valid = true;

                                        ixMax = ix + sx;
                                        iyMax = iy + sy;
                                        channelMax = channel;
                                    }
                                }
                            }
                        }

                        argMax(ox, oy, output, batchPos)
                            = ArgMax(ixMax, iyMax, channelMax, valid);
                    }

                    outputs(ox, oy, output, batchPos)
                        = (*alpha) * poolValue
                          + (*beta) * outputs(ox, oy, output, batchPos);
                }
            }
        }
    }
}

*/

namespace {
static Registrar<MaxPoolingImpl2DForward_cpu> registrarMaxPoolingImpl2DForward_cpu_Float32(
        std::tuple<DataType, DataType>({DataType::Float32, DataType::Float32}),
        Aidge::MaxPoolingImpl2D_cpu_forward_kernel<float, float>);
static Registrar<MaxPoolingImpl2DForward_cpu> registrarMaxPoolingImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32},
        Aidge::MaxPoolingImpl2D_cpu_forward_kernel<int, int>);
static Registrar<MaxPoolingImpl2DForward_cpu> registrarMaxPoolingImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64},
        Aidge::MaxPoolingImpl2D_cpu_forward_kernel<double, double>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MaxPOOLINGIMPL_FORWARD_KERNEL_H_ */
