/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_LNIMPL_H_
#define AIDGE_CPU_OPERATOR_LNIMPL_H_

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/Ln.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include <memory>
#include <vector>

namespace Aidge {
// class Ln_Op;

// compute kernel registry for forward and backward
class LnImplForward_cpu
    : public Registrable<LnImplForward_cpu, std::tuple<DataType, DataType>, void(const std::size_t, const void*, void*)> {
};
class LnImplBackward_cpu
    : public Registrable<LnImplBackward_cpu, std::tuple<DataType, DataType, DataType>, void(const std::size_t, const void*, const void*, void*)> {
};

class LnImpl_cpu : public OperatorImpl {
public:
    LnImpl_cpu(const Ln_Op& op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<LnImpl_cpu> create(const Ln_Op& op) {
        return std::make_unique<LnImpl_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
	
    void forward() override final;

    void backward() override final;
};

namespace {
static Registrar<Ln_Op> registrarLnImpl_cpu("cpu", Aidge::LnImpl_cpu::create);
}
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_LNIMPL_H_ */
