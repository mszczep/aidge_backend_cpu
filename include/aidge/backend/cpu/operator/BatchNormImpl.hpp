/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_
#define AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_

#include <array>
#include <memory>
#include <tuple>
#include <vector>

#include "aidge/backend/OperatorImpl.hpp"
#include "aidge/operator/BatchNorm.hpp"
#include "aidge/utils/Registrar.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

namespace Aidge {
// class BatchNorm_Op;

// compute kernel registry for forward and backward
class BatchNormImpl2DForward_cpu
    : public Registrable<BatchNormImpl2DForward_cpu,
                         std::tuple<DataType, DataType, DataType>,
                         void(float,
                            float,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            const void *,
                            const void *,
                            void *,
                            void *,
                            void *,
                            const bool)> {};
class BatchNormImpl2DBackward_cpu
    : public Registrable<BatchNormImpl2DBackward_cpu,
                         std::tuple<DataType, DataType, DataType>,
                         void(float,
                            float,
                            const std::array<DimSize_t, 4> &,
                            const void *,
                            const void *,
                            const void *,
                            void *,
                            void *,
                            void *)> {};

class BatchNormImpl2D_cpu : public OperatorImpl {
public:
    BatchNormImpl2D_cpu(const BatchNorm_Op<2> &op) : OperatorImpl(op, "cpu") {}

    static std::unique_ptr<BatchNormImpl2D_cpu> create(const BatchNorm_Op<2> &op) {
        return std::make_unique<BatchNormImpl2D_cpu>(op);
    }

    Elts_t getNbRequiredProtected(const IOIndex_t inputIdx) const override final;
    void forward() override;
};

namespace {
// add cpu backend to BatchNorm_Op<2> implementation registry
static Registrar<BatchNorm_Op<2>> registrarBatchNormImpl2D_cpu("cpu", Aidge::BatchNormImpl2D_cpu::create);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_BATCHNORMIMPL_H_ */
