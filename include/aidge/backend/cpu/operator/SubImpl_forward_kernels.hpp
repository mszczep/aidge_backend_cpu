/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_SUBIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_SUBIMPL_FORWARD_KERNEL_H_

#include "aidge/utils/Registrar.hpp"

#include <cstddef>     // std::size_t
#include <cstdint>     // std::int32_t, std::int64_t
#include <vector>

#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/operator/SubImpl.hpp"


namespace Aidge {
template <class I1, class I2, class O>
void SubImpl_cpu_forward_kernel(const std::vector<std::size_t>& input1Dims,
                                const std::vector<std::size_t>& input2Dims,
                                const std::vector<std::size_t>& outputDims,
                                const void* input1_,
                                const void* input2_,
                                void* output_) {

    const I1* input_1 = static_cast<const I1*>(input1_);
    const I2* input_2 = static_cast<const I2*>(input2_);
    O* output = static_cast<O*>(output_);

    size_t totalElements = 1;
    for (size_t dimSize : outputDims) {
        totalElements *= dimSize;
    }

	for (std::size_t oIndex = 0; oIndex < totalElements; ++oIndex)
	{
		std::vector<size_t> indexes = getMultiDimIndices(outputDims, oIndex);
		std::size_t idx1 = getFlattenedIndex(input1Dims, indexes);
		std::size_t idx2 = getFlattenedIndex(input2Dims, indexes);
        output[oIndex] = input_1[idx1] - input_2[idx2];
	}
}

namespace {
static Registrar<SubImplForward_cpu> registrarSubImplForward_cpu_Float32(
        {DataType::Float32, DataType::Float32, DataType::Float32},
        Aidge::SubImpl_cpu_forward_kernel<float, float, float>);
static Registrar<SubImplForward_cpu> registrarSubImplForward_cpu_Float64(
        {DataType::Float64, DataType::Float64, DataType::Float64},
        Aidge::SubImpl_cpu_forward_kernel<double, double, double>);
static Registrar<SubImplForward_cpu> registrarSubImplForward_cpu_Int32(
        {DataType::Int32, DataType::Int32, DataType::Int32},
        Aidge::SubImpl_cpu_forward_kernel<std::int32_t, std::int32_t, std::int32_t>);
static Registrar<SubImplForward_cpu> registrarSubImplForward_cpu_Int64(
        {DataType::Int64, DataType::Int64, DataType::Int64},
        Aidge::SubImpl_cpu_forward_kernel<std::int64_t, std::int64_t, std::int64_t>);
}  // namespace
}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_SUBIMPL_FORWARD_KERNEL_H_ */
