/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_MATMULIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_MATMULIMPL_FORWARD_KERNEL_H_

#include "aidge/backend/cpu/operator/MatMulImpl.hpp"

namespace Aidge {

template <class I, class O>
void MatMulImpl_cpu_forward_kernel(const std::size_t n, const std::size_t k, const std::size_t m,
                                    const void* input1_, const void* input2_, void* output_) {
    // FIXME: missing MatMul parameters as arguments
    const I* input1 = static_cast<const I*>(input1_);
    const I* input2 = static_cast<const I*>(input2_);
    O* output = static_cast<O*>(output_);

    for (std::size_t i = 0; i < n; ++i) {
        for (std::size_t j = 0; j < m; ++j) {
            O sum = O(0);
            for (std::size_t l = 0; l < k; ++l) {
                sum += static_cast<O>(input1[i*k + l] * input2[l*m + j]);
            }
            output[i*m + j] = sum;
        }
    }
}

namespace {
static Registrar<MatMulImplForward_cpu> registrarMatMulImpl2DForward_cpu_Float32(
        {DataType::Float32, DataType::Float32},
        Aidge::MatMulImpl_cpu_forward_kernel<float, float>);
static Registrar<MatMulImplForward_cpu> registrarMatMulImpl2DForward_cpu_Int32(
        {DataType::Int32, DataType::Int32},
        Aidge::MatMulImpl_cpu_forward_kernel<int, int>);
static Registrar<MatMulImplForward_cpu> registrarMatMulImpl2DForward_cpu_Float64(
        {DataType::Float64, DataType::Float64},
        Aidge::MatMulImpl_cpu_forward_kernel<double, double>);
}  // namespace

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_MATMULIMPL_FORWARD_KERNEL_H_ */
