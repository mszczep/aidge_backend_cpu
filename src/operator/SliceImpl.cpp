/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/SliceImpl.hpp"

#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/SliceImpl_forward_kernels.hpp"
#include "aidge/operator/Slice.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::SliceImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::SliceImpl_cpu::forward() {
    const auto& op_ = dynamic_cast<const Slice_Op&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Slice Operator.");

    // Find the correct kernel type
    auto kernelFunc = Registrar<SliceImplForward_cpu>::create({
        op_.getInput(0)->dataType(),
        op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.starts(),
            op_.ends(),
            op_.axes(),
            op_.steps(),
            op_.getInput(0)->dims(),
            getCPUPtr(mOp.getRawInput(0)),
            getCPUPtr(mOp.getRawOutput(0)));
}
