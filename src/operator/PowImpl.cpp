/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Pow.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/Broadcasting.hpp"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

#include "aidge/backend/cpu/operator/PowImpl.hpp"
#include "aidge/backend/cpu/operator/PowImpl_forward_kernels.hpp"

Aidge::Elts_t Aidge::PowImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::PowImpl_cpu::forward() {
    // Find the correct kernel type
    auto kernelFunc = Registrar<PowImplForward_cpu>::create({
        std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
        std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->dataType(),
        std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

    const std::vector<std::size_t> inputDims0 = getBroadcastedDims(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dims(),
                                                                   std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims());
    const std::vector<std::size_t> inputDims1 = getBroadcastedDims(std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dims(),
                                                                   std::static_pointer_cast<Tensor>(mOp.getRawInput(1))->dims());

    // Call kernel
    kernelFunc(inputDims0,
        inputDims1,
        std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dims(),
        getCPUPtr(mOp.getRawInput(0)),
        getCPUPtr(mOp.getRawInput(1)),
        getCPUPtr(mOp.getRawOutput(0)));
}

void Aidge::PowImpl_cpu::backward() {
    // Find the correct kernel type
    const Pow_Op& op_ = dynamic_cast<const Pow_Op&>(mOp);
    auto kernelFunc = Registrar<PowImplForward_cpu>::create({
        op_.getOutput(0)->grad()->dataType(),
        op_.getInput(0)->grad()->dataType(),
        op_.getInput(1)->grad()->dataType()});

    const std::vector<std::size_t> input0gradDims = getBroadcastedDims(op_.getInput(0)->grad()->dims(),
                                                                   op_.getOutput(0)->grad()->dims());
    const std::vector<std::size_t> input1gradDims = getBroadcastedDims(op_.getInput(1)->grad()->dims(),
                                                                   op_.getOutput(0)->grad()->dims());

    // Call kernel
    kernelFunc(op_.getOutput(0)->grad()->dims(),
               input0gradDims,
               input1gradDims,
               getCPUPtr(mOp.getRawOutput(0)),
               getCPUPtr(mOp.getRawInput(0)),
               getCPUPtr(mOp.getRawInput(1)));
}