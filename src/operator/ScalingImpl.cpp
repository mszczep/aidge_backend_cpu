/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <numeric>    // std::accumulate
#include <functional> // std::multiplies
#include <vector>

#include "aidge/operator/Scaling.hpp"

#include "aidge/backend/cpu/operator/ScalingImpl.hpp"
#include "aidge/backend/cpu/operator/ScalingImpl_forward_kernels.hpp"
#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"

Aidge::Elts_t Aidge::ScalingImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::ScalingImpl_cpu::forward() {
    const auto& op_ = dynamic_cast<const Scaling_Op&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Scaling Operator.");

    // Find the correct kernel type
    auto kernelFunc = Registrar<ScalingImplForward_cpu>::create({
        op_.getInput(0)->dataType(),
        op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.scalingFactor(),
            op_.quantizedNbBits(),
            op_.isOutputUnsigned(),
            op_.getInput(0)->size(),
            getCPUPtr(mOp.getRawInput(0)),
            getCPUPtr(mOp.getRawOutput(0)));
}
