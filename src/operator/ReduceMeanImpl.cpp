/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ReduceMeanImpl.hpp"

#include <memory>
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/operator/ReduceMean.hpp"
#include "aidge/backend/cpu/operator/ReduceMeanImpl_forward_kernels.hpp"

void Aidge::ReduceMeanImpl_cpu::forward() {
    const ReduceMean_Op& op_ = dynamic_cast<const ReduceMean_Op&>(mOp);
    // Find the correct kernel type
    auto kernelFunc = Registrar<ReduceMeanImplForward_cpu>::create({
        op_.getInput(0)->dataType(),
        op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.axes(),
                op_.keepDims(),
                op_.getInput(0)->dims(),
                op_.getInput(0)->getImpl()->rawPtr(),
                op_.getOutput(0)->getImpl()->rawPtr());
}

// void Aidge::ReduceMeanImpl1D_cpu::forward() {

//     // Find the correct kernel type
//     auto kernelFunc =
//             Registrar<ReduceMeanImpl1DForward_cpu>::create({
//         std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
//         std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

//     // Call kernel
//     kernelFunc(dynamic_cast<const ReduceMean_Op<1>&>(mOp).getStaticAttributes(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->getImpl()->rawPtr(),
//                std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());
// }

// void Aidge::ReduceMeanImpl2D_cpu::forward() {

//     // Find the correct kernel type
//     auto kernelFunc =
//             Registrar<ReduceMeanImpl2DForward_cpu>::create({
//         std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
//         std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

//     // Call kernel
//     kernelFunc(dynamic_cast<const ReduceMean_Op<2>&>(mOp).getStaticAttributes(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->getImpl()->rawPtr(),
//                std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());
// }

// void Aidge::ReduceMeanImpl3D_cpu::forward() {

//     // Find the correct kernel type
//     auto kernelFunc =
//             Registrar<ReduceMeanImpl3DForward_cpu>::create({
//         std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dataType(),
//         std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->dataType()});

//     // Call kernel
//     kernelFunc(dynamic_cast<const ReduceMean_Op<3>&>(mOp).getStaticAttributes(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->dims(),
//                std::static_pointer_cast<Tensor>(mOp.getRawInput(0))->getImpl()->rawPtr(),
//                std::static_pointer_cast<Tensor>(mOp.getRawOutput(0))->getImpl()->rawPtr());
// }