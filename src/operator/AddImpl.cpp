/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/AddImpl.hpp"

#include <cassert>
#include <numeric> // std::accumulate
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/AddImpl_forward_kernels.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/utils/Types.h"
#include "aidge/utils/ErrorHandling.hpp"

Aidge::Elts_t  Aidge::AddImpl_cpu::getNbRequiredProtected(const Aidge::IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void  Aidge::AddImpl_cpu::forward() {
    const auto& opTensor = static_cast<const OperatorTensor&>(mOp);
    AIDGE_ASSERT(opTensor.getInput(0)->hasImpl(), "cannot run Add forward because the 0-th input has no implementation.");
    assert(opTensor.getInput(0) && "missing input in Add operator");
    DataType datatypeFirstInput = opTensor.getInput(0)->dataType();
    for (IOIndex_t i = 1; i < opTensor.nbInputs(); ++i) {
        AIDGE_ASSERT(opTensor.getInput(i)->hasImpl(), "cannot run Add forward because the {}-th input has no implementation.", i);
        assert(opTensor.getInput(i) && "missing input in Add operator");
        assert(opTensor.getInput(i)->dataType() == datatypeFirstInput);
    }

    // Find the correct kernel type
    const auto outputDataType = opTensor.getOutput(0)->dataType();
    const Registrar<AddImplForward_cpu>::registrar_key registrarKey = {
        datatypeFirstInput,
        outputDataType};

    Registrar<AddImplForward_cpu>::registrar_type kernelFunc;
    if (Registrar<AddImplForward_cpu>::exists(registrarKey)) {
        // One exists with the right inputs/output types
        kernelFunc = Registrar<AddImplForward_cpu>::create(registrarKey);
    }
    else {
        // Otherwise, fallback to the kernel with all types matching output type
        kernelFunc = Registrar<AddImplForward_cpu>::create({
            outputDataType, outputDataType});
    }

    // Convert input data (no overhead if not needed!)
    // TODO: right now, if needed, memory will be allocated/deallocated at each
    // call to forward(). We might put the following shared_ptr as members of
    // this class to avoid that.
    const std::size_t nbDims = opTensor.getOutput(0)->nbDims();
    std::vector<std::vector<std::size_t>> inputsDims;
    std::vector<const void*> opInputs;
    std::vector<std::shared_ptr<Tensor>> inputsFallback(opTensor.nbInputs());
    for (IOIndex_t i = 0; i < opTensor.nbInputs(); ++i) {
        std::vector<std::size_t> inputDims(nbDims, 1);
        auto dims = opTensor.getInput(i)->dims();
		for(std::size_t j=dims.size()-1; j+1>0; --j)
		{
			std::size_t idx = nbDims - (dims.size()-j);
			inputDims[idx] = dims[j];
		}
        inputsDims.push_back(inputDims);
        const auto& input = opTensor.getInput(i)->refCastFrom(inputsFallback[i], *opTensor.getOutput(0));
        opInputs.push_back(input.getImpl()->rawPtr());
    }

    kernelFunc(opInputs,
               inputsDims,
               opTensor.getOutput(0)->size(),
               opTensor.getOutput(0)->dims(),
               getCPUPtr(opTensor.getRawOutput(0)));
}
