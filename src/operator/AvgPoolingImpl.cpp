/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/AvgPoolingImpl.hpp"

#include <array>
#include <numeric>
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/AvgPoolingImpl_forward_kernels.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/AvgPooling.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::AvgPoolingImpl2D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::AvgPoolingImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const AvgPooling_Op<2>&>(mOp);
    assert(op_.getInput(0) && "missing input #0");

    // Find the correct kernel type
    auto kernelFunc = Registrar<AvgPoolingImpl2DForward_cpu>::create(
        {op_.getInput(0)->dataType(),
         op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.strideDims(),
               op_.kernelDims(),
               op_.getInput(0)->template dims<4>(),
               getCPUPtr(op_.getInput(0)),
               getCPUPtr(op_.getOutput(0)));
}
