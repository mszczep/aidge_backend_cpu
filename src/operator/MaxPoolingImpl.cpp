/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/MaxPoolingImpl.hpp"

#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/MaxPoolingImpl_forward_kernels.hpp"
#include "aidge/operator/MaxPooling.hpp"
#include "aidge/utils/Log.hpp"
#include "aidge/utils/Types.h"


Aidge::Elts_t Aidge::MaxPoolingImpl2D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::MaxPoolingImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const MaxPooling_Op<2>&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in MaxPooling Operator.");

    // Find the correct kernel type
    auto kernelFunc = Registrar<MaxPoolingImpl2DForward_cpu>::create({
        op_.getInput(0)->dataType(),
        op_.getOutput(0)->dataType()
    });

    // Call kernel
    kernelFunc(op_.strideDims(),
                op_.kernelDims(),
                op_.ceilMode(),
                op_.getInput(0)->template dims<4>(),
                getCPUPtr(mOp.getRawInput(0)),
                getCPUPtr(mOp.getRawOutput(0)));
}
