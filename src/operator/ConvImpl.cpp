/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/ConvImpl.hpp"

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/backend/cpu/operator/ConvImpl_forward_kernels.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/utils/Types.h"

Aidge::Elts_t Aidge::ConvImpl1D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::ConvImpl1D_cpu::forward() {
    const auto& op_ = static_cast<const Conv_Op<1>&>(mOp);

    // FIXME: uncomment the following code once memory handling will work
AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Conv Operator.");
    AIDGE_ASSERT(op_.getInput(1), "missing input #1 in Conv Operator.");

    // Find the correct kernel type
    const auto outputDataType = op_.getOutput(0)->dataType();
    const Registrar<ConvImpl1DForward_cpu>::registrar_key registrarKey = {
        op_.getInput(0)->dataType(),
        op_.getInput(1)->dataType(),
        (op_.getInput(2) ? op_.getInput(2)->dataType() : op_.getInput(1)->dataType()),
        outputDataType};

    Registrar<ConvImpl1DForward_cpu>::registrar_type kernelFunc;
    if (Registrar<ConvImpl1DForward_cpu>::exists(registrarKey)) {
        // One exists with the right inputs/output types
        kernelFunc = Registrar<ConvImpl1DForward_cpu>::create(registrarKey);
    }
    else {
        // Otherwise, fallback to the kernel with all types matching output type
        kernelFunc = Registrar<ConvImpl1DForward_cpu>::create({
            outputDataType, outputDataType, outputDataType, outputDataType});
    }

    // Convert input data (no overhead if not needed!)
    // TODO: right now, if needed, memory will be allocated/deallocated at each
    // call to forward(). We might put the following shared_ptr as members of
    // this class to avoid that.
    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback;
    const auto& input0 = op_.getInput(0)->refCastFrom(input0Fallback, *op_.getOutput(0));
    const auto& input1 = op_.getInput(1)->refCastFrom(input1Fallback, *op_.getOutput(0));
    const auto& input2 = (op_.getInput(2)) ? op_.getInput(2)->refCastFrom(input2Fallback, *op_.getOutput(0)) : Tensor();

    // Call kernel
    kernelFunc(op_.strideDims(),
            op_.dilationDims(),
            op_.kernelDims(),
            op_.getInput(0)->template dims<3>(), // input dimensions
            dynamic_cast<const Conv_Op<2>&>(mOp).outChannels(), // outChannels
            input0.getImpl()->rawPtr(), // input
            input1.getImpl()->rawPtr(), // weight
            op_.getInput(2) ? input2.getImpl()->rawPtr() : nullptr, // bias
            getCPUPtr(mOp.getRawOutput(0)) // output
            );
}

Aidge::Elts_t Aidge::ConvImpl2D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::ConvImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const Conv_Op<2>&>(mOp);

    // FIXME: uncomment the following code once memory handling will work
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 in Conv Operator.");
    AIDGE_ASSERT(op_.getInput(1), "missing input #1 in Conv Operator.");

    // Find the correct kernel type
    const auto outputDataType = op_.getOutput(0)->dataType();
    const Registrar<ConvImpl2DForward_cpu>::registrar_key registrarKey = {
        op_.getInput(0)->dataType(),
        op_.getInput(1)->dataType(),
        (op_.getInput(2) ? op_.getInput(2)->dataType() : op_.getInput(1)->dataType()),
        outputDataType};

    Registrar<ConvImpl2DForward_cpu>::registrar_type kernelFunc;
    if (Registrar<ConvImpl2DForward_cpu>::exists(registrarKey)) {
        // One exists with the right inputs/output types
        kernelFunc = Registrar<ConvImpl2DForward_cpu>::create(registrarKey);
    }
    else {
        // Otherwise, fallback to the kernel with all types matching output type
        kernelFunc = Registrar<ConvImpl2DForward_cpu>::create({
            outputDataType, outputDataType, outputDataType, outputDataType});
    }

    // Convert input data (no overhead if not needed!)
    // TODO: right now, if needed, memory will be allocated/deallocated at each
    // call to forward(). We might put the following shared_ptr as members of
    // this class to avoid that.
    std::shared_ptr<Tensor> input0Fallback, input1Fallback, input2Fallback;
    const auto& input0 = op_.getInput(0)->refCastFrom(input0Fallback, *op_.getOutput(0));
    const auto& input1 = op_.getInput(1)->refCastFrom(input1Fallback, *op_.getOutput(0));
    const auto& input2 = (op_.getInput(2)) ? op_.getInput(2)->refCastFrom(input2Fallback, *op_.getOutput(0)) : Tensor();

    // Call kernel
    kernelFunc(op_.strideDims(),
            op_.dilationDims(),
            op_.kernelDims(),
            op_.getInput(0)->template dims<4>(), // input dimensions
            dynamic_cast<const Conv_Op<2>&>(mOp).outChannels(), // outChannels
            input0.getImpl()->rawPtr(), // input
            input1.getImpl()->rawPtr(), // weight
            op_.getInput(2) ? input2.getImpl()->rawPtr() : nullptr, // bias
            getCPUPtr(mOp.getRawOutput(0)) // output
            );
}
