/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include "aidge/backend/cpu/operator/BatchNormImpl.hpp"


#include <numeric> // std::accumulate
#include <vector>

#include "aidge/utils/Types.h"
#include "aidge/backend/cpu/data/GetCPUPtr.h"
#include "aidge/operator/BatchNorm.hpp"

#include "aidge/backend/cpu/operator/BatchNormImpl_forward_kernels.hpp"

Aidge::Elts_t Aidge::BatchNormImpl2D_cpu::getNbRequiredProtected(IOIndex_t /*inputIdx*/) const {
    // this implementation can be in-place
    return Elts_t::DataElts(0);
}

void Aidge::BatchNormImpl2D_cpu::forward() {
    const auto& op_ = dynamic_cast<const BatchNorm_Op<2>&>(mOp);
    AIDGE_ASSERT(op_.getInput(0), "missing input #0 for BatchNorm Operator");
    AIDGE_ASSERT(op_.getInput(1), "missing input #1 for BatchNorm Operator");
    AIDGE_ASSERT(op_.getInput(2), "missing input #2 for BatchNorm Operator");
    AIDGE_ASSERT(op_.getInput(3), "missing input #3 for BatchNorm Operator");
    AIDGE_ASSERT(op_.getInput(4), "missing input #4 for BatchNorm Operator");

    AIDGE_ASSERT(op_.getOutput(0)->nbDims() == 4, "");
    // Find the correct kernel type
    auto kernelFunc =
            Registrar<BatchNormImpl2DForward_cpu>::create({op_.getInput(0)->dataType(),
                                                           op_.getInput(1)->dataType(),
                                                           op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(op_.epsilon(),
            op_.momentum(),
            op_.getInput(0)->template dims<4>(),
            getCPUPtr(op_.getRawInput(0)),
            getCPUPtr(op_.getRawInput(1)),
            getCPUPtr(op_.getRawInput(2)),
            getCPUPtr(op_.getRawInput(3)),
            getCPUPtr(op_.getRawInput(4)),
            getCPUPtr(op_.getRawOutput(0)),
            true);
}
