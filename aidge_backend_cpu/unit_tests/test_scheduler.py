import unittest
import aidge_core
import aidge_backend_cpu
import numpy as np


class test_scheduler(unittest.TestCase):
    """Test tensor binding
    """
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_relu_forward(self):
        values = np.arange(6) - 3

        input_node = aidge_core.Producer(aidge_core.Tensor(values), "Input")
        relu = aidge_core.ReLU()

        gv = aidge_core.GraphView()
        gv.add(relu)
        gv.add(input_node)

        input_node.add_child(relu)

        gv.set_datatype(aidge_core.dtype.int32)
        gv.set_backend("cpu")

        scheduler = aidge_core.SequentialScheduler(gv)

        scheduler.forward()

        out_tensor = relu.get_operator().get_output(0)
        expected_out = [0,0,0,0,1,2]
        for i in range(len(expected_out)):
            self.assertEqual(expected_out[i], out_tensor[i])

    def test_sequential_scheduling(self):
        input_data =  np.array([0]).astype(np.float32)
        input_tensor = aidge_core.Tensor(input_data)

        graph_view = aidge_core.sequential([
            aidge_core.Producer(input_tensor, "X"),
            aidge_core.FC(1, 50, name='0'),
            aidge_core.FC(50, 50, name='1'),
            aidge_core.FC(50, 10, name='2'),
        ])
        EXPECTED_SCHEDULE = ['0', '1', '2']

        graph_view.set_datatype(aidge_core.dtype.float32)
        graph_view.set_backend("cpu")

        graph_view.forward_dims()

        scheduler = aidge_core.SequentialScheduler(graph_view)
        scheduler.generate_scheduling()

        self.assertEqual(len(scheduler.get_static_scheduling()), 10)
        # Do not care about the order of execution of the producers
        self.assertListEqual([i.name() for i in scheduler.get_static_scheduling()[-3:]], EXPECTED_SCHEDULE)


    def test_parallel_scheduling(self):
        input_data =  np.array([0]).astype(np.float32)
        input_tensor = aidge_core.Tensor(input_data)

        graph_view = aidge_core.sequential([
            aidge_core.Producer(input_tensor, "X"),
            aidge_core.FC(1, 50, name='0'),
            aidge_core.parallel([aidge_core.FC(50, 50, name='1'), aidge_core.FC(50, 50, name='3')]),
            aidge_core.Add(2, name='2'),
        ])

        EXPECTED_SCHEDULE = [['0', '1', '3', '2'],  ['0', '3', '1', '2']] # Both scheduling are valid !

        graph_view.set_datatype(aidge_core.dtype.float32)
        graph_view.set_backend("cpu")

        graph_view.forward_dims()

        scheduler = aidge_core.SequentialScheduler(graph_view)
        scheduler.generate_scheduling()

        self.assertEqual(len(scheduler.get_static_scheduling()), 11)
        # Do not care about the order of execution of the producers
        self.assertTrue([i.name() for i in scheduler.get_static_scheduling()[-4:]] in EXPECTED_SCHEDULE)

if __name__ == '__main__':
    unittest.main()
