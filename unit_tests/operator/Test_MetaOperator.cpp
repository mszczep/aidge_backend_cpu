/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <cmath>
#include <cstdlib>
#include <memory>

#include "aidge/utils/TensorUtils.hpp"
#include "aidge/backend/cpu/operator/ConvImpl.hpp"
#include "aidge/backend/cpu/operator/PadImpl.hpp"
#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/MetaOperator.hpp"
#include "aidge/operator/MetaOperatorDefs.hpp"
#include "aidge/operator/Pad.hpp"
#include "aidge/operator/Pop.hpp"
#include "aidge/scheduler/SequentialScheduler.hpp"
#include "aidge/scheduler/ParallelScheduler.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] MetaOperator", "[MetaOperator][CPU]") {
  SECTION("PaddedConv(forward)") {
    std::shared_ptr<Tensor> myWeights = std::make_shared<Tensor>(
            Array4D<double, 4, 3, 3, 3>{{{{{6.20986394e-01, 1.19775136e-03, 7.22876095e-02},
                                          {1.16492919e-01, 8.21634093e-02, 1.17413265e-01},
                                          {2.23743494e-01, 3.99495413e-01, 5.55552411e-01}},
                                         {{6.64970077e-01, 9.62199940e-01, 4.87531967e-01},
                                          {6.12586558e-01, 8.09918671e-02, 8.40649383e-01},
                                          {4.15264406e-01, 8.28247138e-01, 1.52301135e-01}},
                                         {{1.76992844e-02, 7.78697112e-01, 8.14531592e-01},
                                          {1.36960611e-01, 4.64806728e-01, 4.85150000e-01},
                                          {4.34776520e-01, 9.51740977e-01, 9.05793799e-01}}},

                                        {{{1.71925246e-02, 1.91082720e-01, 3.67982644e-01},
                                          {1.56806559e-01, 6.22280998e-01, 3.15827594e-01},
                                          {6.04359038e-01, 2.83095947e-01, 6.11168892e-01}},
                                         {{2.76942832e-01, 1.89768419e-01, 8.07988176e-01},
                                          {1.67925807e-01, 2.68356150e-01, 6.28875602e-01},
                                          {1.69093357e-04, 9.64788636e-01, 7.29254981e-01}},
                                         {{6.34030122e-01, 1.32087038e-01, 3.33857107e-01},
                                          {7.63047502e-01, 5.12539506e-02, 9.77400493e-01},
                                          {8.06151288e-01, 2.60237147e-01, 3.93729313e-01}}},

                                        {{{5.84605240e-01, 4.74648725e-01, 8.54111741e-01},
                                          {7.10897067e-02, 5.02579011e-01, 3.35236224e-01},
                                          {9.08637408e-01, 8.02903830e-01, 2.83929907e-01}},
                                         {{3.68206999e-01, 9.18579021e-02, 7.33168098e-01},
                                          {1.59875539e-01, 9.13163381e-01, 3.59806060e-01},
                                          {1.41295882e-01, 7.00312185e-01, 5.63728289e-01}},
                                         {{9.39513546e-01, 1.91704891e-01, 1.11454944e-01},
                                          {5.46298282e-01, 2.89698587e-01, 2.62612651e-01},
                                          {1.18554992e-01, 4.32147376e-02, 7.53016994e-01}}},

                                        {{{9.53179175e-01, 2.05041054e-02, 1.11318451e-01},
                                          {8.67878485e-01, 2.93263422e-01, 8.03912714e-01},
                                          {8.93620255e-01, 1.37831128e-01, 3.83640583e-01}},
                                         {{3.96020188e-01, 6.24959320e-01, 1.90709175e-01},
                                          {5.80538620e-01, 6.63031275e-01, 2.07247191e-01},
                                          {5.65672171e-01, 5.57014317e-01, 9.26909496e-01}},
                                         {{3.43901418e-01, 4.47741636e-01, 6.59249367e-01},
                                          {7.34639028e-01, 2.84957200e-02, 9.70225217e-01},
                                          {1.33578790e-02, 6.12054702e-01, 9.36685235e-02}}}}});
    std::shared_ptr<Tensor> myBias = std::make_shared<Tensor>(
            Array1D<double, 4>{{0.16884905, 0.27994487, 0.57227465, 0.06435205}});
    std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array4D<double, 2, 3, 5, 5>{
            // NCHW
            {{{{0.43224481, 0.9047832, 0.18402257, 0.06162838, 0.52490127},
               {0.27773404, 0.55402353, 0.9485062, 0.31197083, 0.80328607},
               {0.85065842, 0.88226201, 0.54971951, 0.23360494, 0.53907884},
               {0.33423098, 0.79564312, 0.80419414, 0.76839638, 0.87248221},
               {0.77328729, 0.65749407, 0.47277589, 0.32889198, 0.93970518}},

              {{0.66669145, 0.64193351, 0.45315988, 0.32794057, 0.38461822},
               {0.72295814, 0.18395073, 0.85909664, 0.30010301, 0.56065865},
               {0.34777938, 0.77869746, 0.33159421, 0.19540932, 0.77767906},
               {0.5778391, 0.08218411, 0.27758371, 0.99017749, 0.61827997},
               {0.10440745, 0.3197831, 0.89157608, 0.12216887, 0.950232}},

              {{0.68073443, 0.2681118, 0.51848834, 0.62864493, 0.36717478},
               {0.64106244, 0.43779425, 0.02771029, 0.78275231, 0.45693104},
               {0.6487417, 0.01603838, 0.73869997, 0.96494221, 0.39588782},
               {0.5975827, 0.90913292, 0.55036969, 0.4747373, 0.62460509},
               {0.79675124, 0.02807549, 0.53227602, 0.88805927, 0.96646591}}},

             {{{0.81851935, 0.21267665, 0.01580692, 0.54907998, 0.89010049},
               {0.80165784, 0.55195592, 0.20740314, 0.22782844, 0.89205031},
               {0.94217108, 0.58434542, 0.20738313, 0.79065873, 0.9371597},
               {0.02254708, 0.95539178, 0.95165758, 0.53736666, 0.49100362},
               {0.08018625, 0.69108027, 0.00329741, 0.74565761, 0.30899213}},

              {{0.34868638, 0.12792604, 0.37382248, 0.0374756, 0.50653087},
               {0.59614405, 0.64820746, 0.31470307, 0.62460364, 0.29253268},
               {0.92864889, 0.51014224, 0.08921206, 0.11094072, 0.64691121},
               {0.50586371, 0.6686477, 0.72511169, 0.41681783, 0.6325049},
               {0.71594137, 0.73382767, 0.36589439, 0.03255165, 0.75006865}},

              {{0.6294127, 0.85548534, 0.0902963, 0.28915773, 0.36564289},
               {0.95873236, 0.6742374, 0.55679676, 0.6323497, 0.34072958},
               {0.49694061, 0.79173045, 0.19738225, 0.14755281, 0.80818177},
               {0.02332061, 0.74270703, 0.59415632, 0.08195934, 0.46295434},
               {0.71426058, 0.85032931, 0.90750818, 0.28768431, 0.4401146}}}}});

    std::shared_ptr<Tensor> myOutput = std::make_shared<Tensor>(
            Array4D<double, 2, 4, 5, 5>{{{{{3.40294218, 3.74021220, 4.02050114, 4.07054710, 2.46286273},
                {4.61770582, 6.70517588, 6.50356627, 6.29688787, 3.53332567},
                {5.47480106, 5.92094421, 6.64605665, 7.95090199, 4.28721523},
                {4.01485729, 6.06748962, 7.52447891, 7.37980652, 5.28401136},
                {2.83065438, 3.62033439, 3.56222963, 5.56103945, 3.23335814}},

                {{3.30230498, 4.92814112, 4.34710836, 3.96262765, 2.97987890},
                {4.49693012, 6.68929291, 5.53603029, 5.68874264, 4.28756475},
                {4.20528078, 6.82776880, 6.70569849, 7.12809610, 4.40845442},
                {4.31169367, 6.73352146, 6.30962515, 7.45826864, 4.99164438},
                {2.18136287, 4.28968000, 4.20080042, 4.89814138, 2.87394023}},

                {{3.54787683, 4.35851812, 4.63881302, 4.23359537, 3.16992092},
                {5.25099468, 7.54282856, 6.69849157, 5.64309788, 4.56919575},
                {4.71914101, 7.52830601, 6.71450949, 7.81113863, 5.84658146},
                {4.97893143, 7.39293909, 6.89905310, 8.14430809, 5.62998581},
                {2.79735112, 4.80967140, 5.57630205, 5.38828325, 4.57078695}},

                {{3.03048635, 5.04540300, 4.21824932, 4.87323284, 2.35113740},
                {4.45167351, 6.47721338, 7.40922976, 6.70445728, 3.60700107},
                {3.77927423, 6.82826376, 7.41777134, 7.57402420, 5.13131523},
                {4.08747244, 7.07994175, 7.57206821, 8.51897335, 5.26987123},
                {2.34426999, 4.60127831, 4.86486769, 6.01579571, 3.97803569}}},


                {{{3.84700942, 4.25972605, 3.05269003, 3.78043652, 2.08771229},
                {6.00459957, 6.05633259, 4.45951605, 4.54089880, 4.03066444},
                {5.41579390, 7.29543972, 6.18680000, 5.58812714, 3.45964241},
                {6.04531050, 7.70924091, 5.52207708, 5.02131319, 4.09403706},
                {3.18092418, 4.45422697, 4.04294252, 3.86577177, 2.18776536}},

                {{4.02600670, 4.27603531, 3.81011319, 4.03631020, 2.57254648},
                {5.33471155, 5.72588634, 5.12079763, 5.11733150, 3.76836705},
                {5.62947607, 5.92492962, 6.24170446, 6.44130468, 3.44276404},
                {5.38414621, 6.02679539, 5.88985586, 5.90263271, 3.15044069},
                {3.31261086, 4.44371319, 3.47660780, 4.15411520, 1.48961508}},

                {{3.95879412, 4.17324543, 3.70114422, 3.27447152, 3.09713888},
                {5.78258181, 6.57920837, 4.99913597, 6.20961237, 4.98552179},
                {5.84685421, 7.19971228, 6.66386652, 6.68013430, 4.90963316},
                {5.24417877, 7.06430531, 6.58512402, 6.02492285, 4.48986387},
                {3.64294529, 5.00678444, 5.04760027, 4.72895622, 2.67990756}},

                {{3.48610687, 4.12853813, 4.07563591, 3.51327014, 2.44217038},
                {4.80529881, 7.33211374, 5.14774036, 4.77281189, 4.44612408},
                {5.11703110, 7.55168772, 7.14374542, 6.43696356, 4.10621357},
                {5.41270018, 6.85949135, 6.73503923, 5.74601364, 4.46150303},
                {3.16612267, 4.38248920, 5.23248482, 4.21292210, 2.86031270}}}}});

    std::shared_ptr<Node> myConv = Conv<2>(3, 4, {3, 3}, "myconv");
    auto convOp = std::static_pointer_cast<OperatorTensor>(myConv->getOperator());

    std::shared_ptr<Node> myPad =
            Pad<2>({1, 1, 1, 1}, "myPad", PadBorderType::Constant, 0.0);
    auto padOp = std::static_pointer_cast<OperatorTensor>(myPad->getOperator());

    convOp->setInput(1, myWeights);
    convOp->setInput(2, myBias);

    myPad->addChild(myConv, 0, 0);
    padOp->setInput(0, myInput);

    padOp->setDataType(DataType::Float64);
    padOp->setBackend("cpu");
    convOp->setDataType(DataType::Float64);
    convOp->setBackend("cpu");

    myPad->forward();
    myConv->forward();
    convOp -> getOutput(0) -> print();

    double* computedOutput = static_cast<double*>(convOp->getOutput(0)->getImpl()->rawPtr());
    double* expectedOutput = static_cast<double*>(myOutput->getImpl()->rawPtr());
    for (std::size_t i = 0; i < myOutput->size(); ++i) {
        REQUIRE(std::abs(computedOutput[i] - expectedOutput[i]) < 1e-5);
    }

    std::shared_ptr<Node> myPaddedConv =
            PaddedConv(3, 4, {3, 3}, "myPaddedConv", {1, 1}, {1, 1, 1, 1});
  }
    SECTION("LSTM(forward)") {
        auto pop = Pop();
        auto myLSTM = LSTM(32, 64, 0, true, "ltsm");
        auto op = std::dynamic_pointer_cast<MetaOperator_Op>(myLSTM->getOperator());

        auto microGraph = op->getMicroGraph();
        microGraph->save("lstm", false, true);

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->inputCategory(0) == InputCategory::Data);
        for (size_t i = 1; i < 9; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::Param);
        }
        for (size_t i = 9; i < 17; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::OptionalParam);
        }
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array2D<float, 16, 32>{});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 32, 64>{});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 64, 32>{});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 64, 64>{});

        pop->addChild(myLSTM, 0, 0);
        pop->getOperator()->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        myLSTM->input(1).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(2).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(3).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(4).first->getOperator()->setOutput(0, myInitW);
        // Weights H
        myLSTM->input(5).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(6).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(7).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(8).first->getOperator()->setOutput(0, myInitR);

        auto g = getConnectedGraphView(myLSTM);
        g->setDataType(DataType::Float32);
        g->setBackend("cpu");

        auto scheduler = SequentialScheduler(g);
        scheduler.forward(true);

        g->save("lstm_outside_dims", true, true);

        microGraph->save("lstm_dims", true, true);
        REQUIRE(op->dimsForwarded());

        auto microGraphScheduler = std::dynamic_pointer_cast<MetaOperator_Op>(op)->getMicroGraphScheduler();
        microGraphScheduler->saveSchedulingDiagram("lstm_scheduling");

        REQUIRE(op->getNbConsumedData(0).data == 512);
        REQUIRE(op->getNbConsumedData(1).data == 32768);
        REQUIRE(op->getNbProducedData(0).data == 34816);
        REQUIRE(op->getNbProducedData(1).data == 34816);
        REQUIRE(microGraphScheduler->getStaticScheduling(0).size() == 26);
        REQUIRE(microGraphScheduler->getStaticScheduling(1).size() == 24);
        REQUIRE(microGraphScheduler->getStaticScheduling(15).size() == 24);
    }
    SECTION("LSTM(forward_values)") {
        auto myLSTM = LSTM(2, 3, 0, true, "ltsm");
        auto op = std::static_pointer_cast<OperatorTensor>(myLSTM->getOperator());

        auto microGraph = std::dynamic_pointer_cast<MetaOperator_Op>(op)->getMicroGraph();
        microGraph->save("lstm", false, false);

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->inputCategory(0) == InputCategory::Data);
        for (size_t i = 1; i < 9; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::Param);
        }
        for (size_t i = 9; i < 17; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::OptionalParam);
        }
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{1.0, 2.0}, {3.0, 4.0}, {5.0, 6.0}}});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{0.1, 0.1}, {0.1, 0.1}, {0.1, 0.1}}});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}}});

        op->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        myLSTM->input(1).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(2).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(3).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(4).first->getOperator()->setOutput(0, myInitW);
        // Weights H
        myLSTM->input(5).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(6).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(7).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(8).first->getOperator()->setOutput(0, myInitR);

        auto g = getConnectedGraphView(myLSTM);
        g->setDataType(DataType::Float32);
        g->setBackend("cpu");

        auto scheduler = SequentialScheduler(g);
        scheduler.forward();

        microGraph->save("lstm_values_dims", false, true);

        std::shared_ptr<Tensor> myHiddenState = std::make_shared<Tensor>(
                Array2D<float, 3, 3>{{{0.0952412, 0.0952412, 0.0952412},
                                     {0.25606447, 0.25606447, 0.25606447},
                                     {0.40323776, 0.40323776, 0.40323776}}});


        auto microGraphScheduler = std::dynamic_pointer_cast<MetaOperator_Op>(op)->getMicroGraphScheduler();
        microGraphScheduler->saveSchedulingDiagram("lstm_values_scheduling");

        op->getOutput(0)->print();
        myHiddenState->print();

        REQUIRE(approxEq<float>(*(op->getOutput(0)), *myHiddenState));
    }
    SECTION("LSTM(forward_values_seq)") {
        auto pop = Pop();
        auto myLSTM = LSTM(2, 3, 2, true, "ltsm");
        auto myGraph = Sequential({pop, myLSTM});
        auto op = std::static_pointer_cast<OperatorTensor>(myLSTM->getOperator());

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->inputCategory(0) == InputCategory::Data);
        for (size_t i = 1; i < 9; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::Param);
        }
        for (size_t i = 9; i < 17; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::OptionalParam);
        }
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array3D<float, 2, 3, 2>{{{{1.0, 2.0}, {3.0, 4.0}, {5.0, 6.0}}, {{2.0, 3.0}, {4.0, 5.0}, {6.0, 7.0}}}});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{0.1, 0.1}, {0.1, 0.1}, {0.1, 0.1}}});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}}});

        pop->getOperator()->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        myLSTM->input(1).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(2).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(3).first->getOperator()->setOutput(0, myInitW);
        myLSTM->input(4).first->getOperator()->setOutput(0, myInitW);
        // Weights H
        myLSTM->input(5).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(6).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(7).first->getOperator()->setOutput(0, myInitR);
        myLSTM->input(8).first->getOperator()->setOutput(0, myInitR);

        auto g = getConnectedGraphView(myLSTM);
        g->compile("cpu", DataType::Float32);

        g->save("lstm_seq", true, true);

        auto scheduler = SequentialScheduler(g);
        scheduler.forward();
        scheduler.saveSchedulingDiagram("lstm_seq_schedule");

        std::shared_ptr<Tensor> myHiddenState = std::make_shared<Tensor>(
                Array2D<float, 3, 3>{{{0.24439372, 0.24439372, 0.24439372},
                                     {0.49801484, 0.49801484, 0.49801484},
                                     {0.67162132, 0.67162132, 0.67162132}}});

        myGraph->save("lstm_seq_mygraph", true, true);

        op->getOutput(0)->print();
        myHiddenState->print();

        REQUIRE(approxEq<float>(*(op->getOutput(0)), *myHiddenState));
    }
    SECTION("LSTM(forward_values_seq_flatten)(sequential)") {
        auto pop = Pop();
        auto myLSTM = LSTM(2, 3, 2, true, "ltsm");
        auto op = std::static_pointer_cast<MetaOperator_Op>(myLSTM->getOperator());

        // Here we test LSTM as it is was flatten in the graph.
        // We just borrow its micro-graph into our larger myGraph graph.
        auto myGraph = std::make_shared<GraphView>();
        pop->addChild(op->getMicroGraph()->getOrderedInputs()[0].first, 0, 0);
        myGraph->add(op->getMicroGraph());
        myGraph->add(pop);

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->inputCategory(0) == InputCategory::Data);
        for (size_t i = 1; i < 9; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::Param);
        }
        for (size_t i = 9; i < 17; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::OptionalParam);
        }
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array3D<float, 2, 3, 2>{{{{1.0, 2.0}, {3.0, 4.0}, {5.0, 6.0}}, {{2.0, 3.0}, {4.0, 5.0}, {6.0, 7.0}}}});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{0.1, 0.1}, {0.1, 0.1}, {0.1, 0.1}}});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}}});

        pop->getOperator()->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        auto prodX = Producer(myInitW);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[1].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[2].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[3].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[4].first, 0, 1);
        // Weights H
        auto prodH = Producer(myInitR);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[5].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[6].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[7].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[8].first, 0, 1);
        myGraph->add({prodX, prodH});

        myGraph->setDataType(DataType::Float32);
        myGraph->setBackend("cpu");
        myGraph->save("lstm_seq_flatten", true, true);

        std::shared_ptr<Tensor> myHiddenState = std::make_shared<Tensor>(
                Array2D<float, 3, 3>{{{0.24439372, 0.24439372, 0.24439372},
                                     {0.49801484, 0.49801484, 0.49801484},
                                     {0.67162132, 0.67162132, 0.67162132}}});

        auto scheduler = SequentialScheduler(myGraph);
        scheduler.generateScheduling();
        scheduler.saveStaticSchedulingDiagram("lstm_static_schedule");
        scheduler.forward(true);
        scheduler.saveSchedulingDiagram("lstm_seq_flatten_schedule_seq");

        op->getOutput(0)->print();
        myHiddenState->print();

        REQUIRE(approxEq<float>(*(op->getOutput(0)), *myHiddenState));
    }
    SECTION("LSTM(forward_values_seq_flatten)(parallel)") {
        auto pop = Pop();
        auto myLSTM = LSTM(2, 3, 2, true, "ltsm");
        auto op = std::static_pointer_cast<MetaOperator_Op>(myLSTM->getOperator());

        // Here we test LSTM as it is was flatten in the graph.
        // We just borrow its micro-graph into our larger myGraph graph.
        auto myGraph = std::make_shared<GraphView>();
        pop->addChild(op->getMicroGraph()->getOrderedInputs()[0].first, 0, 0);
        myGraph->add(op->getMicroGraph());
        myGraph->add(pop);

        REQUIRE(myLSTM->nbInputs() == 3 + 8 + 8);
        REQUIRE(myLSTM->inputCategory(0) == InputCategory::Data);
        for (size_t i = 1; i < 9; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::Param);
        }
        for (size_t i = 9; i < 17; ++i) {
            REQUIRE(myLSTM->inputCategory(i) == InputCategory::OptionalParam);
        }
        REQUIRE(myLSTM->nbOutputs() == 2);

        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(
            Array3D<float, 2, 3, 2>{{{{1.0, 2.0}, {3.0, 4.0}, {5.0, 6.0}}, {{2.0, 3.0}, {4.0, 5.0}, {6.0, 7.0}}}});
        std::shared_ptr<Tensor> myInit = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}});
        std::shared_ptr<Tensor> myInitW = std::make_shared<Tensor>(
            Array2D<float, 3, 2>{{{0.1, 0.1}, {0.1, 0.1}, {0.1, 0.1}}});
        std::shared_ptr<Tensor> myInitR = std::make_shared<Tensor>(
            Array2D<float, 3, 3>{{{0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}, {0.1, 0.1, 0.1}}});

        pop->getOperator()->associateInput(0, myInput);
        op->associateInput(17, myInit);
        op->associateInput(18, myInit);

        // Weights X
        auto prodX = Producer(myInitW);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[1].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[2].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[3].first, 0, 1);
        prodX->addChild(op->getMicroGraph()->getOrderedInputs()[4].first, 0, 1);
        // Weights H
        auto prodH = Producer(myInitR);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[5].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[6].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[7].first, 0, 1);
        prodH->addChild(op->getMicroGraph()->getOrderedInputs()[8].first, 0, 1);
        myGraph->add({prodX, prodH});

        myGraph->setDataType(DataType::Float32);
        myGraph->setBackend("cpu");
        myGraph->save("lstm_seq_flatten", true, true);

        std::shared_ptr<Tensor> myHiddenState = std::make_shared<Tensor>(
                Array2D<float, 3, 3>{{{0.24439372, 0.24439372, 0.24439372},
                                     {0.49801484, 0.49801484, 0.49801484},
                                     {0.67162132, 0.67162132, 0.67162132}}});

        auto scheduler = ParallelScheduler(myGraph);
        scheduler.generateScheduling();
        scheduler.forward(true);
        scheduler.saveSchedulingDiagram("lstm_seq_flatten_schedule_par");

        op->getOutput(0)->print();
        myHiddenState->print();

        REQUIRE(approxEq<float>(*(op->getOutput(0)), *myHiddenState));
    }
}