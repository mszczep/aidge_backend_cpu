/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/LeakyReLU.hpp"

#include "aidge/backend/cpu.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] LeakyReLU(forward)", "[LeakyReLU][CPU]") {
    SECTION("1D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<int,10> {
            {0, 1, 2,-3, 4,-5,-6, 7, 8, 9}
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<int,10> {
            {0, 1, 2, 0, 4, 0, 0, 7, 8, 9}
        });

        std::shared_ptr<Node> myLeakyReLU = LeakyReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myLeakyReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myLeakyReLU->forward();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }

    SECTION("2D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array2D<int,2,10> {
            {
                { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array2D<int,2,10> {
            {
                { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
            }
        });

        std::shared_ptr<Node> myLeakyReLU = LeakyReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myLeakyReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myLeakyReLU->forward();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array3D<int,2,2,10> {
            {
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                },
                {
                    { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                    {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array3D<int,2,2,10> {
            {
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                },
                {
                    { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                    { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                }
            }
        });

        std::shared_ptr<Node> myLeakyReLU = LeakyReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myLeakyReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myLeakyReLU->forward();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }

    SECTION("4D Tensor") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                },
                {
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    },
                    {
                        { 0, 1, 2,-3, 4,-5,-6, 7, 8, 9},
                        {-5, 4, 2,-3, 4,-5,-6, 7,-1,10}
                    }
                }
            }
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array4D<int,2,2,2,10> {
            {
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                },
                {
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    },
                    {
                        { 0, 1, 2, 0, 4, 0, 0, 7, 8, 9},
                        { 0, 4, 2, 0, 4, 0, 0, 7, 0,10}
                    }
                }
            }
        });

        std::shared_ptr<Node> myLeakyReLU = LeakyReLU();
        auto op = std::static_pointer_cast<OperatorTensor>(myLeakyReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Int32);
        op->setBackend("cpu");
        myLeakyReLU->forward();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }

    SECTION("Test construction attribute: negative_slop") {
        std::shared_ptr<Tensor> input0 = std::make_shared<Tensor>(Array1D<float,10> {
            {0.0f, 1.0f, 2.0f,-3.0f, 4.0f,-5.0f,-6.0f, 7.0f, 8.0f, 9.0f}
        });
        std::shared_ptr<Tensor> expectedOutput = std::make_shared<Tensor>(Array1D<float,10> {
            {0.0f, 1.0f, 2.0f,-1.5f, 4.0f,-2.5f,-3.0f, 7.0f, 8.0f, 9.0f}
        });

        std::shared_ptr<Node> myLeakyReLU = LeakyReLU(0.5f);
        auto op = std::static_pointer_cast<OperatorTensor>(myLeakyReLU -> getOperator());
        op->associateInput(0,input0);
        op->setDataType(DataType::Float32);
        op->setBackend("cpu");
        myLeakyReLU->forward();
        REQUIRE(*(op->getOutput(0)) == *expectedOutput);
    }
}