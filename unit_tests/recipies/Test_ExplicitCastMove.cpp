/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>

#include "aidge/recipes/Recipes.hpp"
#include "aidge/operator/Conv.hpp"
#include "aidge/operator/Producer.hpp"
#include "aidge/graph/OpArgs.hpp"
#include <cstddef>

using namespace Aidge;

TEST_CASE("[ExplicitCastMove] conv") {
    auto conv1 = Conv(3, 32, {3, 3}, "conv1");
    auto conv2 = Conv(32, 64, {3, 3}, "conv2");
    auto conv3 = Conv(64, 10, {1, 1}, "conv3", {2, 2});

    auto g1 = Sequential({
        Producer({16, 3, 224, 224}, "dataProvider"),
        conv1,
        conv2,
        conv3
    });

    g1->setBackend("cpu");
    conv1->getOperator()->setDataType(DataType::Int32);
    conv3->getOperator()->setDataType(DataType::Float64);

    g1->save("explicitCastMove_before");
    REQUIRE(g1->getNodes().size() == 10);

    g1->forwardDims();
    explicitCastMove(g1);

    g1->save("explicitCastMove_after");
    REQUIRE(g1->getNodes().size() == 13);
}
